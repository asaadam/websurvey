export default {
    items_sample: [
        { name: 'Home', url: '/home', icon: 'icon-home' },
        { title: true, name: 'Sample Layout' },
        { name: 'Page 1', url: '/sample/page1', icon: 'icon-drop' },
        { name: 'Page 2', url: '/sample/page2', icon: 'icon-drop' },
        { title: true, name: 'Ant UI' },
        {
            name: 'Forms',
            url: '/forms',
            icon: 'icon-notebook',
            children: [
                { name: 'Simple Form', url: '/forms/simple', icon: 'icon-tag' },
                { name: 'Binding Form', url: '/forms/binding', icon: 'icon-tag' },
                { name: 'Validation Form', url: '/forms/validation', icon: 'icon-tag' },
            ]
        },
        {
            name: 'Ant Upload',
            url: '/upload/ant',
            icon: 'icon-cloud-upload',
            children: [
                { name: 'Simple Uploads', url: '/upload/ant/simple', icon: 'icon-tag' },
                { name: 'Gallery Uploads', url: '/upload/ant/gallery', icon: 'icon-tag' },
                { name: 'Thumbnail Uploads', url: '/upload/ant/thumb', icon: 'icon-tag' },
            ]
        },
        {
            name: 'Ant Grid',
            url: '/grid/ant',
            icon: 'icon-grid',
            children: [
                { name: 'Simple Grid', url: '/grid/ant/simple', icon: 'icon-tag' },
            ]
        },
        {
            name: 'Editor',
            url: '/editor',
            icon: 'icon-pencil',
            children: [
                { name: 'Simple Quill', url: '/editor/simple', icon: 'icon-tag' },
                { name: 'More Toolbar', url: '/editor/toolbar', icon: 'icon-tag' },
                { name: 'Using Wrapper', url: '/editor/wrapper', icon: 'icon-tag' },
            ]
        },
        {
            name: 'CRUD',
            url: '/crud',
            icon: 'icon-grid',
            children: [
                { name: 'Master Kategori', url: '/crud/simple', icon: 'icon-tag' },
            ]
        },
    ],
    items: []
};
