import React from 'react';

const routes = [
  { path: '/:id', component: React.lazy(() => import('../views/Home')) },

  { path: '/content', exact: true, component: React.lazy(() => import('../views/contents')) },
  { path: '/content/page1', component: React.lazy(() => import('../views/contents/Page1')) },
  { path: '/content/page2', component: React.lazy(() => import('../views/contents/Page2')) },
  { path: '/content/page3', component: React.lazy(() => import('../views/contents/Page3')) },
  { path: '/content/page4', component: React.lazy(() => import('../views/contents/Page4')) },

  { path: '/comps/text', component: React.lazy(() => import('../views/comps/InputText')) },
  { path: '/comps/textarea', component: React.lazy(() => import('../views/comps/InputTextArea')) },
  { path: '/comps/select', component: React.lazy(() => import('../views/comps/InputSelect')) },
  { path: '/comps/action', component: React.lazy(() => import('../views/comps/InputAction')) },
  { path: '/comps/check-image', component: React.lazy(() => import('../views/comps/CheckImage')) },
  { path: '/comps/radio-image', component: React.lazy(() => import('../views/comps/RadioImage')) },
  { path: '/comps/tab-panel', component: React.lazy(() => import('../views/comps/TabPanel')) },

  { path: '/forms/simple', component: React.lazy(() => import('../views/forms/SimpleForm')) },
  { path: '/forms/binding', component: React.lazy(() => import('../views/forms/BindingForm')) },
  { path: '/forms/validation', component: React.lazy(() => import('../views/forms/ValidationForm')) },

  { path: '/upload/ant/simple', component: React.lazy(() => import('../views/uploads/ant/SimpleUploads')) },
  { path: '/upload/ant/gallery', component: React.lazy(() => import('../views/uploads/ant/GalleryUploads')) },
  { path: '/upload/ant/thumb', component: React.lazy(() => import('../views/uploads/ant/ThumbUploads')) },

  { path: '/grid/ant/simple', component: React.lazy(() => import('../views/grids/ant/SimpleGrid')) },

  { path: '/editor/simple', component: React.lazy(() => import('../views/editors/SimpleQuill')) },
  { path: '/editor/toolbar', component: React.lazy(() => import('../views/editors/MoreToolbar')) },
  { path: '/editor/wrapper', component: React.lazy(() => import('../views/editors/UsingWrapper')) },

  { path: '/crud/simple', component: React.lazy(() => import('../views/cruds/SimpleCrud')), exact: true },
  { path: '/crud/simple/add', component: React.lazy(() => import('../views/cruds/SimpleCrud/FormAdd')) },
  { path: '/crud/simple/:id', component: React.lazy(() => import('../views/cruds/SimpleCrud/FormEdit')) },

  { path: '/authors/dashboard', component: React.lazy(() => import('../views/authors/Dashboard')) },
];

export default routes;
