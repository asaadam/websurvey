import axios from 'axios';
import store from '../stores';

const apiUrl = process.env.REACT_APP_BASE_URL;

const validateToken = async () => {
    // TODO: Validate Token
    return true;
};

const getHeaders = (refreshToken) => {
    const { auth = {} } = store.getState();
    const { attributes } = auth;
    const headers = {
        'Accept': 'application/vnd.api+json',
        'Content-Type': 'application/vnd.api+json',
        'Access-Control-Allow-Origin': true,
        'x-api-key': '87df9e1b-0715-4bc9-9370-8f18619080d8'
    };

    if (!refreshToken && attributes && attributes.accessToken) {
        headers.Authorization = `Bearer ${attributes.accessToken}`;
    }

    return headers;
};

const getOption = (method, url, data = {}) => {
    return { headers: getHeaders(data.refreshToken), method, url: `${apiUrl}/${url}`, data };
};

const app = {
    get: async (url) => {
        await validateToken();
        const option = getOption('get', url);
        const { data } = await axios.request(option);
        return data;
    },
    post: async (url, params) => {
        await validateToken();
        const option = getOption('post', url, params);
        const { data } = await axios.request(option);
        return data;
    },
    patch: async (url, params) => {
        await validateToken();
        const option = getOption('patch', url, params);
        const { data } = await axios.request(option);
        return data;
    },
    put: async (url, params) => {
        await validateToken();
        const option = getOption('put', url, params);
        const { data } = await axios.request(option);
        return data;
    },
    delete: async (url, params) => {
        await validateToken();
        const option = getOption('delete', url, params);
        const { data } = await axios.request(option);
        return data;
    }
};

export default app;