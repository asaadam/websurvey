import {
    AUTH_SIGNIN_USER_SUCCESS,
    AUTH_SIGNOUT_USER_SUCCESS,
    SHOW_MESSAGE,
    HIDE_MESSAGE,
    ON_HIDE_LOADER,
    ON_SHOW_LOADER,
} from '../../constants/ActionTypes';

const storeState = 'moco_auth';
const initState = {
    loader: false,
    alertMessage: '',
    showMessage: false,
    initURL: '',
    authUser: JSON.parse(localStorage.getItem(storeState)),
}

export default (state = initState, action) => {
    const { type, payload } = action;

    switch (type) {
        case AUTH_SIGNIN_USER_SUCCESS: {
            return {
                ...state,
                loader: false,
                authUser: payload
            }
        }
        case AUTH_SIGNOUT_USER_SUCCESS: {
            return {
                ...state,
                authUser: null,
                initURL: '/',
                loader: false
            }
        }
        case SHOW_MESSAGE: {
            return {
                ...state,
                alertMessage: action.payload,
                showMessage: true,
                loader: false
            }
        }
        case HIDE_MESSAGE: {
            return {
                ...state,
                alertMessage: '',
                showMessage: false,
                loader: false
            }
        }
        case ON_SHOW_LOADER: {
            return {
                ...state,
                loader: true
            }
        }
        case ON_HIDE_LOADER: {
            return {
                ...state,
                loader: false
            }
        }
        // case AUTH_SET: {
        //     localStorage.setItem(storeState, JSON.stringify(payload));
        //     return { ...payload }
        // }
        // case AUTH_SET_TOKEN: {
        //     attributes = { ...state.attributes, ...payload };
        //     localStorage.setItem(storeState, JSON.stringify({ ...state, attributes }));
        //     return { ...state, attributes }
        // }
        // case AUTH_CLEAR:
        //     localStorage.removeItem(storeState);
        //     return {}
        default:
            return state;
    }
}
