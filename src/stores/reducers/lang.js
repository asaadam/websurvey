import {
    LANG_SET,
} from '../../constants/ActionTypes';

const initState = {
    code: 'id',
    list: [
        { code: 'eng', text: 'English' },
        { code: 'id', text: 'Indonesia' },
    ]
}

export default (state = initState, action) => {
    const { type, payload } = action;

    switch (type) {
        case LANG_SET: {
            return { ...state, code: payload }
        }
        default:
            return state;
    }
}
