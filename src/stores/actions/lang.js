import i18next from 'i18next';
import { LANG_SET } from '../../constants/ActionTypes';

export const changeLanguage = (value) => {
    i18next.changeLanguage(value).then(() => {
        i18next.options.lng = value;
    });

    return {
        type: LANG_SET,
        payload: value
    };
};

export default { changeLanguage };