import {
    AUTH_SET,
    AUTH_SET_TOKEN,
    AUTH_CLEAR,
    AUTH_SIGNIN_USER,
    AUTH_SIGNIN_USER_SUCCESS,
    AUTH_SIGNOUT_USER,
    AUTH_SIGNOUT_USER_SUCCESS,
    SHOW_MESSAGE,
} from '../../constants/ActionTypes';

export const setAuth = (value) => {
    return {
        type: AUTH_SET,
        payload: value
    };
};
export const setToken = (value) => {
    return {
        type: AUTH_SET_TOKEN,
        payload: value
    };
};
export const clearAuth = (value) => {
    return {
        type: AUTH_CLEAR,
        payload: value
    };
};


export const userSignIn = (user) => {
    return {
        type: AUTH_SIGNIN_USER,
        payload: user
    };
};
export const userSignInSuccess = (authUser) => {
    return {
        type: AUTH_SIGNIN_USER_SUCCESS,
        payload: authUser
    }
};
export const userSignOut = () => {
    return {
        type: AUTH_SIGNOUT_USER
    };
};
export const userSignOutSuccess = () => {
    return {
        type: AUTH_SIGNOUT_USER_SUCCESS,
    }
};
export const showAuthMessage = (message) => {
    return {
        type: SHOW_MESSAGE,
        payload: message
    };
};


export default { setAuth, userSignIn, setToken, clearAuth };