import React, { Suspense, useState, useEffect } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as router from 'react-router-dom';
import { userSignOut } from '../../stores/actions/auth';

import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';

import oApi from '../../providers/firebase';
import navigation from '../../routes/menu';
import routes from '../../routes';

const MocoFooter = React.lazy(() => import('./MocoFooter'));
const MocoHeader = React.lazy(() => import('./MocoHeader'));
const PageNotFound = React.lazy(() => import('../../views/pages/NotFound'));

export default (props) => {
  const [state, setState] = useState({ menus: navigation });

  const dispatch = useDispatch();
  const loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>;
  const signOut = async (e) => {
    dispatch(userSignOut());
  };

  useEffect(() => {
    oApi.getDataRef('menus').on('value', (snapshot) => {
      const values = snapshot.val();
      setState({ ...state, menus: values });
    });

    return () => {
      oApi.getDataRef('menus').off('value');
    };
  }, []);

  return (
    <div className="app">
      <div className="app-body">
        <main className="main">
          <Suspense fallback={loading()}>
            <Switch>
              {routes.map((route, idx) => {
                return route.component ? (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={props => (
                      <route.component {...props} />
                    )} />
                ) : (null);
              })}
              <Redirect exact from="/" to="/home" />
              <Route render={() => <PageNotFound />} />
            </Switch>
          </Suspense>
        </main>
      </div>
      <AppFooter style={{ marginTop: 28 }}>
        <MocoFooter />
      </AppFooter>
    </div>
  );
};
