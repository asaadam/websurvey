import React from 'react';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import './index.scss';

// pages
const MocoPage = React.lazy(() => import('./MocoLayout'));
const LoginPage = React.lazy(() => import('../../views/pages/Login'));

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;
const ProtectedRoute = ({ authUser }) => {
    return (
        <Route
            render={props => authUser
                ? <MocoPage {...props} />
                : <Redirect to={{ pathname: "/login" }} />}
        />
    );
};

const App = ({ auth }) => {
    return (
        <Router>
            <React.Suspense fallback={loading()}>
                <Switch>
                    <Route exact path="/login" name="Login Page" render={m => <LoginPage {...m} />} />
                    <Route path="/" render={props => <MocoPage {...props} />} />
                </Switch>
            </React.Suspense>
        </Router>
    );
};

const mapStateToProps = ({ auth }) => ({ auth });
export default connect(mapStateToProps)(App);