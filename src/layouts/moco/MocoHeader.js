import React from 'react';
import { connect } from 'react-redux';
import { UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import { changeLanguage } from '../../stores/actions/lang';

import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from './img/brand/logo.png';

const App = (props) => {
  const { auth, lang } = props;

  return (
    <React.Fragment>
      <AppSidebarToggler className="d-lg-none" display="md" mobile />
      <AppNavbarBrand style={{ marginLeft: '20px', width: 195 }}
        full={{ src: logo, height: 60, alt: 'Company Logo' }}
        minimized={{ src: logo, height: 30, alt: 'Company Logo' }}
      />
      <AppSidebarToggler className="d-md-down-none" display="lg" />
      <Nav className="d-md-down-none" navbar>
        <NavItem className="px-3">
        </NavItem>
      </Nav>
      <Nav className="ml-auto mr-2" navbar>
        <UncontrolledDropdown nav direction="down">
          <DropdownToggle nav>
            {`${lang.code.toUpperCase()}`}
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem onClick={() => props.changeLanguage('id')} >ID</DropdownItem>
            <DropdownItem onClick={() => props.changeLanguage('en')} >EN</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>

        <UncontrolledDropdown nav direction="down">
          <DropdownToggle nav className="ml-4 mr-4">
            {auth.email}
            {/* <img src={'../../assets/img/avatars/1.jpg'} className="img-avatar" alt="admin@dashboard.com" /> */}
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
            <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
            <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
            <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
            <DropdownItem onClick={e => props.onLogout(e)}><i className="fa fa-lock"></i> Logout</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Nav>
    </React.Fragment>
  );
}

const mapState = ({ auth, lang }) => ({ auth: auth.authUser, lang });
export default connect(mapState, { changeLanguage })(App);