export default {
  demo: {
    configs: {
      content: {
        page1: {
          breadcrumbs: [
            { text: "Home", to: "/" },
            { text: "Layout" },
            { text: "Simple Content" }
          ],
          title: "Simple Content"
        },
        page2: {
          breadcrumbs: [
            { text: "Home", to: "/" },
            { text: "Layout" },
            { text: "Content with subtitle" }
          ],
          subtitle: "Sample Content with subtitle",
          title: "Content"
        },
        page3: {
          breadcrumbs: [
            { text: "Home", to: "/" },
            { text: "Layout" },
            { text: "Content with Toolbars" }
          ],
          subtitle: "Sample Content with toolbars",
          title: "Content",
          toolbars: [
            { text: "Add", type: "primary", icon: "plus" },
            { text: "Edit", type: "primary", icon: "edit" },
            { text: "Delete", type: "danger", icon: "trash" }
          ]
        }
      },
      comps: {
        text: {
          first_name: {
            label: "Nama Depan",
            rules: [
              { required: true, message: "Silahkan input Nama depan" }
            ]
          },
          last_name: {
            label: "Nama Belakang",
            rules: [
              { min: 3, message: "Minimum 3 huruf" }
            ]
          },
          full_name: {
            label: "Nama Lengkap"
          }
        },
        textarea: {
          memo1: { text: "Memo 1", type: "textarea" },
          memo2: { text: "Memo 2", type: "textarea" },
          memo3: { text: "Description", type: "editor" }
        }
      }
    },
    menus: {
      items: [
        { icon: "icon-home", name: "Home", url: "/home" },
        { name: "Layout", title: true },
        { icon: "icon-drop", name: "Content Simple", url: "/content/page1" },
        { icon: "icon-drop", name: "Content with Subtitle", url: "/content/page2" },
        { icon: "icon-drop", name: "Content with Toolbars", url: "/content/page3" },
        { icon: "icon-drop", name: "Content without Header", url: "/content/page4" },

        { name: "Component UI", title: true },
        { icon: "icon-drop", name: "Input Text", url: "/comps/text" },
        { icon: "icon-drop", name: "Input Text Area", url: "/comps/textarea" },
        { icon: "icon-drop", name: "Input Select", url: "/comps/select" },
        { icon: "icon-drop", name: "Input Action", url: "/comps/action" },
        { icon: "icon-drop", name: "Checkbox Image", url: "/comps/check-image" },
        { icon: "icon-drop", name: "Radio Image", url: "/comps/radio-image" },

        { name: "Sample UI", title: true },
        {
          icon: "icon-notebook",
          name: "Authoring Tools",
          url: "/authors",
          children: [
            { icon: "icon-grid", name: "Dashboard", url: "/authors/dashboard" },
          ]
        }
        // {
        //   children: [
        //     { icon: "icon-tag", name: "Simple Form", url: "/forms/simple" },
        //     { icon: "icon-tag", name: "Binding Form", url: "/forms/binding" },
        //     { icon: "icon-tag", name: "Validation Form", url: "/forms/validation" }
        //   ],
        //   icon: "icon-notebook",
        //   name: "Forms",
        //   url: "/forms"
        // },
        // {
        //   children: [
        //     { icon: "icon-tag", name: "Simple Uploads", url: "/upload/ant/simple" },
        //     { icon: "icon-tag", name: "Gallery Uploads", url: "/upload/ant/gallery" },
        //     { icon: "icon-tag", name: "Thumbnail Uploads", url: "/upload/ant/thumb" }
        //   ],
        //   icon: "icon-cloud-upload",
        //   name: "Ant Upload",
        //   url: "/upload/ant"
        // },
        // {
        //   children: [
        //     { icon: "icon-tag", name: "Simple Grid", url: "/grid/ant/simple" }
        //   ],
        //   icon: "icon-grid",
        //   name: "Ant Grid",
        //   url: "/grid/ant"
        // },
        // {
        //   children: [
        //     { icon: "icon-tag", name: "Simple Quill", url: "/editor/simple" },
        //     { icon: "icon-tag", name: "More Toolbar", url: "/editor/toolbar" },
        //     { icon: "icon-tag", name: "Using Wrapper", url: "/editor/wrapper" }
        //   ],
        //   icon: "icon-pencil",
        //   name: "Editor",
        //   url: "/editor"
        // },
        // {
        //   children: [
        //     { icon: "icon-tag", name: "Master Kategori", url: "/crud/simple" }
        //   ],
        //   icon: "icon-grid",
        //   name: "CRUD",
        //   url: "/crud"
        // }
      ]
    }
  }
}