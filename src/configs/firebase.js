// import firebase from "firebase";
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyAup4Wja3dhbf1IUCypB2N7CnCfO2QyD3w',
  authDomain: 'moco-core-sample.firebaseapp.com',
  databaseURL: 'https://moco-core-sample.firebaseio.com',
  projectId: 'moco-core-sample',
  storageBucket: 'moco-core-sample.appspot.com',
  messagingSenderId: '896939178998',
  appId: '1:896939178998:web:705a2878c468ae692ba5d7',
  measurementId: 'G-Q3SL2YNVPR'
};

firebase.initializeApp(config);
const auth = firebase.auth();

const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const githubAuthProvider = new firebase.auth.GithubAuthProvider();

const database = firebase.database();
const storage = firebase.storage();

export {
  database,
  storage,
  auth,
  googleAuthProvider,
  githubAuthProvider,
  facebookAuthProvider,
};
