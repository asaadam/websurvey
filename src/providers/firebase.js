import { database } from '../configs/firebase';

const APP_NAME = process.env.REACTAPP_NAME || 'demo';

export default {
    getData: async (modl) => {
        const resp = await database.ref(`${APP_NAME}/${modl}`).once('value');
        return resp.val();
    },
    getDataRef: (modl) => {
        return database.ref(`${APP_NAME}/${modl}`);
    },
    setData: async (modl, value) => {
        const data = { ...value };
        delete data.key;
        return database.ref(`${APP_NAME}/${modl}`).set(data);
    },
    getConfigRef: (modl) => {
        return database.ref(`${APP_NAME}/configs/${modl}`);
    },
    setConfig: async (modl, value) => {
        const data = { ...value };
        delete data.key;
        return database.ref(`${APP_NAME}/configs/${modl}`).set(data);
    },
}
