import axios from 'axios';

const baseUrl = 'https://api.gsooka.com';

export default {
    getDropdown: async (name) => {
        try {
            const { data } = await axios.get(`${baseUrl}/coronavirus/${name}`);
            return data;
        } catch (err) {
            throw err;
        }
    },
}
