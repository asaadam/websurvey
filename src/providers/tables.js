import request from '../utils/request';

export default {
  list: (resource) => {
    return request.get(`tables/${resource}`);
  },
  update: (resource, attributes) => {
    return request.patch(`tables/${resource}`, { data: { "type": "author", attributes } });
  },
  insert: (resource, attributes) => {
    return request.post(`tables/${resource}`, { data: { attributes } });
  },
  delete: (resource) => {
    return request.delete(`tables/${resource}`);
  },
};