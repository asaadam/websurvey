import authAction from '../stores/actions/auth';
import request from '../utils/request';
import store from '../stores';

export default {
    login: async (params) => {
        // const url = 'services/auth';
        // const option = { provider: 'local', data: params };
        // const resp = await request.post(url, option);
        // authAction.setAuth(resp.data);
        // return resp;

        // using kae auth
        const resp = {
            type: 'LoginExisting',
            id: '28dc27d3-8a08-4698-b24c-23e233c533a0',
            attributes: {
                refreshToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
                accessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9',
                provider: 'local',
                email: 'admin@mylib.id',
                verified: true,
                accessTokenExpired: '2020-02-25 18:59:27 +07:00',
                refreshTokenExpired: '2020-03-02 15:12:08 +07:00',
            }
        };
        authAction.setAuth(resp);
        return resp;
    },
    refreshToken: async () => {
        const { auth } = store.getState();
        const { attributes = {} } = auth;

        if (attributes.refreshToken) {
            const { data } = await request.patch('services/auth', { refreshToken: attributes.refreshToken });
            authAction.setToken(data.attributes);
            return data.attributes;
        }

        return attributes;
    },
    logout: () => {
        authAction.clearAuth();
        return Promise.resolve();
    }
};