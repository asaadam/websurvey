import request from '../utils/request';

export default {
  list: (resource) => {
    return request.get(`webhook/${resource}`);
  },
  update: (resource, data) => {
    return request.put(`webhook/${resource}`, data);
  },
  insert: (resource, data) => {
    return request.post(`webhook/${resource}`, data);
  },
  delete: (resource) => {
    return request.delete(`webhook/${resource}`);
  },
};