import data from './configs/data';
import oData from './providers/firebase';

const demo = data.demo;

export default () => {
    oData.setData('menus', demo.menus);
    oData.setData('configs', demo.configs);
}