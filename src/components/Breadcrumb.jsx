import React from 'react';
import { Link } from 'react-router-dom';

const Breadcrumb = (props) => {
    if (props.items) {
        return (
            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    {props.items.map((m, idx) => {
                        if (m.to) {
                            return (
                                <li key={idx} className="breadcrumb-item">
                                    <Link to={m.to}>{m.text}</Link>
                                </li>
                            )
                        } else {
                            return (
                                <li key={idx} className="breadcrumb-item active" aria-current="page">{m.text}</li>
                            )
                        }
                    })}
                </ol>
            </nav>
        )
    } else {
        return (
            <div className="mt-4"></div>
        );
    }
}

export default Breadcrumb;