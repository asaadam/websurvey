import React from 'react';
import { Table } from 'antd';

const MocoTable = (props) => {
  return (
    <Table rowKey={row => row.id} {...props} />
  );
}

export default MocoTable;