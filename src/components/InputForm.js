import React, { useEffect } from 'react';
import { Row, Col, Form } from 'antd';

import Content from './Content';
import InputControl from './InputControl';

const App = (props) => {
  const { scope, controls = [], onChange, form, model } = props;
  const defCol = props.defCol || { lg: { span: 16 }, xl: { span: 14 } }

  useEffect(() => {
    form.setFieldsValue(model);
  }, []);

  return (
    <Content {...scope}>
      <Form>
        <Row gutter={12}>
          {controls.map((prop, idx) => {
            if (props.schema) props.schema.form = form;
            return (
              <Col key={idx} {...(prop.defcol || defCol)}>
                <InputControl  {...prop} schema={props.schema} onChange={onChange} />
              </Col>
            )
          })}
        </Row>
        {props.children}
      </Form>
    </Content>
  );
};

export default Form.create({ name: `FORM_${Date.now()}` })(App);