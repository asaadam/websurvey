import React from 'react';
import { Form, DatePicker } from 'antd';
import translator from 'i18next';

export default (props) => {
    const { onChange, name, schema = {}, format, showTime, extra } = props;
    const defaultFormat = 'dddd DD-MM-YYYY';

    const handleChange = (value, e) => {
        if (onChange) {
            onChange(name, value, e);
        }
    };

    if (name && schema[name]) {
        const fieldSchema = schema[name];
        const label = props.text || fieldSchema.label;
        return (
            <Form.Item
                extra={extra}
                name={name}
                label={translator.t(label)}
                rules={fieldSchema.rules}
                style={{ marginBottom: 5 }}
            >
                <DatePicker format={format || defaultFormat} rows={4} {...props} onChange={handleChange} showTime={showTime} />
            </Form.Item>
        );
    }
    return (
        <Form.Item style={{ marginBottom: 5 }}>
            {props.text ? (<strong>{translator.t(props.text)}</strong>) : ''}
            <DatePicker format={format || defaultFormat} rows={4} {...props} style={{ width: '100%' }} onChange={handleChange} showTime={showTime} />
        </Form.Item>
    );
};
