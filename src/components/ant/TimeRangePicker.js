import React from 'react'
import { Form, TimePicker } from 'antd'

import translator from 'i18next'
import map from 'lodash/map'

const { RangePicker } = TimePicker

export default props => {
    const { onChange, name, schema = {}, format } = props
    const defaultFormat = 'HH:mm'

    const handleChange = (value, e) => {
        if (onChange) {
            onChange(name, value, e)
        }
    }

    if (name && schema[name]) {
        const fieldSchema = schema[name]
        const label = props.text || fieldSchema.label

        return (
            <Form.Item
                name={name}
                label={translator.t(label)}
                rules={map(fieldSchema.rules, rule => ({
                    ...rule,
                    message: translator.t(rule.message)
                }))}
                style={{ marginBottom: 5 }}
            >
                <RangePicker
                    format={format || defaultFormat}
                    rows={4}
                    {...props}
                    placeholder={map(props.placeholder, holder => translator.t(holder))}
                    onChange={handleChange}
                />
            </Form.Item>
        )
    }

    return (
        <Form.Item style={{ marginBottom: 5 }}>
            {props.text ? <strong>{translator.t(props.text)}</strong> : ''}
            <RangePicker
                format={format || defaultFormat}
                rows={4}
                {...props}
                style={{ width: '100%' }}
                onChange={handleChange}
            />
        </Form.Item>
    )
}
