import React from 'react';
import { Form, InputNumber } from 'antd';

import translator from 'i18next';
import map from 'lodash/map';

export default (props) => {
  const { onChange, name, schema = {}, extra } = props;

  const handleChange = (value, e) => {
    if (name && onChange) {
      onChange(name, value, e);
    }
  };
  if (name && schema[name]) {
    const fieldSchema = schema[name];
    const label = props.text || fieldSchema.label;

    return (
      <Form.Item
        extra={extra}
        name={name}
        label={translator.t(label)}
        rules={map(fieldSchema.rules, rule => ({
          ...rule,
          message: translator.t(rule.message)
        }))}
        style={{ marginBottom: 5 }}
      >
        <InputNumber
          {...props}
          style={{ width: "100%" }}
          onChange={handleChange}
        />
      </Form.Item>
    );
  }

  return (
    <Form.Item style={{ marginBottom: 5 }}>
      <strong>{translator.t(props.text)}&nbsp;</strong>
      <InputNumber {...props} style={{ width: '100%' }} onChange={handleChange} />
    </Form.Item>
  );
};

