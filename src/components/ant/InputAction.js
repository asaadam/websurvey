import React from 'react';
import { Form, Button } from 'antd';

export default (props) => {
  const { onChange, name } = props;
  const useLabel = props.useLabel || props.text;

  const handleAction = (action) => {
    if (name && onChange) {
      onChange(name, action);
    }
  }

  return (
    <Form.Item style={{ marginBottom: 5 }}>
      {useLabel ? (<div><strong>{props.text || '\u00A0'}</strong></div>) : ''}
      {props.actions.map((m, idx) => <Button key={m.key || idx} {...m} onClick={() => handleAction(m.action)}>{m.text}</Button>)}
    </Form.Item>
  );
};