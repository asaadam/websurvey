import React from 'react';
import { Form, Input } from 'antd';

import translator from 'i18next';
import map from 'lodash/map';

export default (props) => {
  const { onChange, name, schema = {}, extra } = props;
  const { TextArea } = Input;

  const handleChange = (value, e) => {
    if (onChange) {
      onChange(name, value, e);
    }
  };

  if (name && schema[name]) {
    const fieldSchema = schema[name];
    const label = props.text || fieldSchema.label;

    return (
      <Form.Item
        extra={extra}
        name={name}
        label={translator.t(label)}
        rules={map(fieldSchema.rules, rule => ({
          ...rule,
          message: translator.t(rule.message)
        }))}
        style={{ marginBottom: 5 }}
      >
        <TextArea rows={4} {...props} onChange={handleChange} />
      </Form.Item>
    );
  }

  return (
    <Form.Item style={{ marginBottom: 5 }}>
      {props.text ? (<strong>{translator.t(props.text)}</strong>) : ''}
      <TextArea rows={4} {...props} style={{ width: '100%' }} onChange={handleChange} />
    </Form.Item>
  );
};

