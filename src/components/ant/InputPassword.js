import React from 'react'; 
import { Form, Input } from 'antd'; 
import translator from 'i18next';
import { LockOutlined } from '@ant-design/icons';

export default (props) => {
    const { onChange, name, schema = {} } = props;
    const handleChange = (e) => {
        if (name && onChange) {
            onChange(name, e.target.value, e);
        }
    }
    const fieldSchema = schema[name];
    const label = props.text || fieldSchema.label;
    return (
        <Form.Item
            name={name}
            label={translator.t(label)}
            rules={fieldSchema.rules}
            style={{ marginBottom: 5 }}
        >
            {/*
                cant place label here, will break form function such as setFieldsValue from Form.useForm
                can only place one children
            */}
            <Input.Password
                {...props}
                onChange={handleChange}
                prefix={<LockOutlined className="site-form-item-icon" />}
            />
        </Form.Item>
    )
};
