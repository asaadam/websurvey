import React from 'react';
import { Form, Radio } from 'antd';

import translator from 'i18next';
import map from 'lodash/map';

export default (props) => {
    const { onChange, name, schema = {}, data = [], buttonStyle, extra } = props;

    const handleChange = (value, e) => {
        // console.log(value, e);
        if (onChange) {
            onChange(name, value, e);
        }
    };

    if (name && schema[name]) {
        const fieldSchema = schema[name];
        const label = props.text || fieldSchema.label;
        return (
            <Form.Item
                extra={extra}
                name={name}
                label={translator.t(label)}
                rules={map(fieldSchema.rules, rule => ({
                    ...rule,
                    message: translator.t(rule.message)
                }))}
                style={{ marginBottom: 5 }}
            >
                <Radio.Group {...props} buttonStyle={buttonStyle || 'solid'} style={{ width: '100%' }} onChange={handleChange}>
                    {data.map((m, idx) => (
                        <Radio.Button key={idx} value={m.value} disabled={m.disabled}>{translator.t(m.text)}</Radio.Button>
                    ))}
                </Radio.Group>
            </Form.Item>
        );
    }

    return (
        <Form.Item style={{ marginBottom: 5 }}>
            {props.text ? (<strong>{translator.t(props.text)}</strong>) : ''}
            <Radio.Group {...props} buttonStyle={buttonStyle || 'solid'} style={{ width: '100%' }} onChange={handleChange}>
                {data.map((m, idx) => (
                    <Radio.Button key={idx} value={m.value} disabled={m.disabled}>{m.text}</Radio.Button>
                ))}
            </Radio.Group>
        </Form.Item>
    );
};

