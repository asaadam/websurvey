import React from 'react';
import { Form, Checkbox } from 'antd';

export default (props) => {
    const { name, onChange, schema = {}, extra } = props;

    const fieldSchema = schema[name] || {};
    const label = fieldSchema.label || fieldSchema.text;
    const { rules } = fieldSchema;

    return (
        <Form.Item name={name} label={label} rules={rules} extra={extra}>
            <Checkbox {...props} onChange={onChange}>{props.text}</Checkbox>
        </Form.Item>
    );
};