import React from 'react' 
import { Form, Upload, Button } from 'antd' 
import { UploadOutlined } from '@ant-design/icons' 
import translator from 'i18next' 
import map from 'lodash/map'


export default props => {
    const { name, schema = {} } = props

    if (name && schema[name]) {
        const fieldSchema = schema[name]
        const label = props.text || fieldSchema.label
        return (
            <Form.Item
                name={name}
                label={translator.t(label)}
                rules={map(fieldSchema.rules, rule => ({
                    ...rule,
                    message: translator.t(rule.message)
                }))}
                style={{ marginBottom: 5 }}
            >
                <Upload {...props}>
                    <Button icon={<UploadOutlined />}> Select File</Button>
                </Upload>
            </Form.Item>
        )
    }
    return (
        <Form.Item style={{ marginBottom: 5 }}>
            {props.text ? <strong>{translator.t(props.text)}</strong> : ''}
            <Upload {...props}>
                <Button icon={<UploadOutlined />}> Select File</Button>
            </Upload>
        </Form.Item>
    )
}
