import React from 'react' 
import { Form, Upload } from 'antd' 
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons' 
import translator from 'i18next' 
import map from 'lodash/map'

export default props => {
    const { name, imgUrl, imgLoading, schema = {} } = props

    const uploadButton = (
      <div>
        {imgLoading ? <LoadingOutlined /> : <PlusOutlined />}
        <div className="ant-upload-text">Upload</div>
      </div>
    )

    if (name && schema[name]) {
        const fieldSchema = schema[name]
        const label = props.text || fieldSchema.label
        return (
            <Form.Item
                name={name}
                label={translator.t(label)}
                rules={map(fieldSchema.rules, rule => ({
                    ...rule,
                    message: translator.t(rule.message)
                }))}
                style={{ marginBottom: 5 }}
            >
                <Upload {...props}>
                    {imgUrl ? <img src={imgUrl} alt="image" style={{ width: '100%' }} /> : uploadButton}
                </Upload>
            </Form.Item>
        )
    }
    return (
        <Form.Item style={{ marginBottom: 5 }}>
            {props.text ? <strong>{translator.t(props.text)}</strong> : ''}
            <Upload {...props}>
              {imgUrl ? <img src={imgUrl} alt="image" style={{ width: '100%' }} /> : uploadButton}
            </Upload>
        </Form.Item>
    )
}
