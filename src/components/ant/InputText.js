import React from 'react';
import { Form, Input } from 'antd';

import translator from 'i18next';
import map from 'lodash/map';

export default props => {

    const { onChange, name, schema = {}, extra } = props;
    const handleChange = e => {
        if (name && onChange) {
            onChange(name, e.target.value, e);
        }
    };

    if (name && schema[name]) {
        const fieldSchema = schema[name];
        const label = props.text || fieldSchema.label;

        return (
            <Form.Item
                name={name}
                label={translator.t(label)}
                rules={map(fieldSchema.rules, rule => ({
                    ...rule,
                    message: translator.t(rule.message)
                }))}
                style={{ marginBottom: 5 }}
                extra={extra}
            >
                {/* 
                cant place label here, will break form function such as setFieldsValue from Form.useForm 
                can only place one children 
                */}
                <Input {...props} onChange={handleChange} />
            </Form.Item>
        );
    }

    return (
        <Form.Item style={{ marginBottom: 5 }} >
            <strong>{translator.t(props.text)}&nbsp;</strong>
            <Input {...props} onChange={handleChange} />
        </Form.Item>
    );
};

