import React from 'react' 
import { Form, Input } from 'antd' 
import InputNumber from './InputNumber' 
import InputText from './InputText' 
import translator from 'i18next' 

export default props => {
    const {
        disabled,
        name,
        schema = {},
        placeholderSeparator = '-',
        inputProps
    } = props
    const fieldSchema = schema[name]
    const label = props.text || fieldSchema.label
    return (
        <Form.Item
            name={name}
            label={<span><span style={{ color: 'red' }}>*</span> {translator.t(label)}</span>}
            style={{ marginBottom: 5 }}
        >
            {/*
                cant place label here, will break form function such as setFieldsValue from Form.useForm
                can only place one children
            */}
            <Input.Group compact>
                <InputNumber
                    className='input-range'
                    disabled={disabled}
                    name={`${name}_min`}
                    {...inputProps}
                />
                <InputText
                    className='input-range-separator'
                    disabled
                    label=''
                    placeholder={placeholderSeparator}
                />
                <InputNumber
                    disabled={disabled}
                    className='input-range last-field'
                    name={`${name}_max`}
                    {...inputProps}
                />
            </Input.Group>
        </Form.Item>
    )
}
