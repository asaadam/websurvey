import React from 'react';
import { Form, Select, Tooltip } from 'antd';
import translator from 'i18next';
import map from 'lodash/map';
export default (props) => {
    const { onChange, name, schema = {}, data = [], extra } = props;
    const { Option } = Select;

    const handleChange = (value, e) => {
        if (onChange) {
            onChange(name, value, e);
        }
    };

    if (name && schema[name]) {
        const fieldSchema = schema[name];
        const label = props.text || fieldSchema.label;
        return (
            <Form.Item
                name={name}
                label={translator.t(label)}
                rules={map(fieldSchema.rules, rule => ({
                    ...rule,
                    message: translator.t(rule.message)
                }))}
                style={{ marginBottom: 5 }}
                extra={extra}
            >
                <Select {...props} onChange={handleChange}>
                    {data.map((m, idx) => (
                        <Option key={idx} value={m.value} disabled={m.disabled}>{m.text}</Option>
                    ))}
                </Select>
            </Form.Item>
        );
    }
    return (
        <Form.Item style={{ marginBottom: 5 }}>
            {props.text ? (<strong>{translator.t(props.text)}</strong>) : ''}
            <Select {...props} onChange={handleChange}>
                {data.map((m, idx) => {
                    return (
                        <Option key={idx} value={m.value} disabled={m.disabled} title={m.text}>{m.text}</Option>
                    );
                })}
            </Select>
        </Form.Item>
    );
};
