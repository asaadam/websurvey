import React from 'react';
import { Card, CardBody, CardHeader, Container } from 'reactstrap';
import { Button } from 'antd';

const Header = (props) => {
    let resp = '';
    if (props.title || props.subtitle) {
        return (
            <CardHeader>
                {(props.title) ? (<h3>{props.title}</h3>) : ''}
                {(props.subtitle) ? (<div>{props.subtitle}</div>) : ''}
            </CardHeader>
        )
    }
    return resp;
}

const Toolbars = (props) => {
    const toolbars = props.toolbars || [];

    if (props.toolbars) {
        return (
            <div className="card-toolbars text-right" style={{ marginRight: 20, marginTop: -47 }}>
                {toolbars.map((m, idx) => {
                    const { onClick } = m;

                    const handleClick = () => {
                        if (onClick) {
                            onClick({ ...props, ...m });
                        }
                    }

                    return (
                        <Button className="mr-1" key={idx} {...m} onClick={handleClick}>{m.text}</Button>
                    )
                })}
            </div>
        )
    }

    return '';
}

const Content = (props = {}) => {
    return (
        <React.Fragment>
            <Container fluid>
                <div className="page-content animated fadeIn">
                    <Card>
                        <Header {...props} />
                        <Toolbars {...props} />
                        <CardBody>{props.children}</CardBody>
                    </Card>
                </div>
            </Container>
        </React.Fragment>
    );
}

export default Content;