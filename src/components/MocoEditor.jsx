import React from 'react';
import { Form } from 'antd';
import ReactQuill from 'react-quill';

const Editor = (props) => {
  console.log(props);

  const modules = {
    toolbar: {
      container: [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ header: 1 }, { header: 2 }],               // custom button values
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ script: 'sub' }, { script: 'super' }],      // superscript/subscript
        [{ indent: '-1' }, { indent: '+1' }],          // outdent/indent
        [{ direction: 'rtl' }],                         // text direction

        [{ size: ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ header: [1, 2, 3, 4, 5, 6, false] }],

        [{ color: [] }, { background: [] }],          // dropdown with defaults from theme
        [{ font: [] }],
        [{ align: [] }],

        ['link', 'latex', 'rumus'],
        ['clean'],
      ],
      handlers: {
        latex(value) {
          console.log('--- latex ---', value);
          // const $this = this;
          // const { quill } = $this;
          // const selection = quill.getSelection();

          // $latex.open({ quill, selection });
        },
        rumus(value) {
          // const $this = this;
          // const { quill } = $this;
          // const selection = quill.getSelection();

          // $formula.open({ quill, selection });
          console.log('--- rumus ---', value)
        }
      }
    },
    formula: true
  }

  const { name, schema = {} } = props;
  const fieldSchema = schema[name] || {};
  const label = fieldSchema.label || fieldSchema.text;
  const { rules } = fieldSchema;

  // if (props.display) {
  //   return (
  //     <React.Fragment>
  //       <div className="ql-editor">
  //         <span dangerouslySetInnerHTML={{ __html: props.value }} />
  //       </div>
  //     </React.Fragment>
  //   )
  // }

  // if (props.text) {
  //   return (
  //     <Form.Item name={name} label={label} rules={rules}>
  //       <ReactQuill modules={modules} {...props}
  //         ref={(el) => {
  //           if (props.setRefs && props.name && el) {
  //             props.setRefs((state) => ({ ...state, [props.name]: el }))
  //           }
  //         }}
  //       />
  //     </Form.Item>
  //   )
  // }

  return (
    <Form.Item name={name} label={label} rules={rules}>
      <ReactQuill modules={modules} {...props} />
    </Form.Item>
  );
}

export default Editor;
