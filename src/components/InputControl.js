import React from 'react';

import InputText from './ant/InputText';
import InputSelect from './ant/InputSelect';
import InputTextArea from './ant/InputTextArea';
import InputNumber from './ant/InputNumber';
import InputPassword from './ant/InputPassword';
import InputRange from './ant/InputRange';
import RadioButton from './ant/RadioButton';
import TimeRangePicker from './ant/TimeRangePicker';
import DatePicker from './ant/DatePicker';
import Upload from './ant/Upload';
import UploadImage from './ant/UploadImage';
import InputCheckbox from './ant/test';
export default (props) => {
  const { type } = props;
  const prop = { ...props };

  delete prop.type;

  switch (type) {
    case 'select':
      return <InputSelect {...prop} />;
    case 'textarea':
      return <InputTextArea {...prop} />;
    case 'number':
      return <InputNumber {...prop} />;
    case 'password':
      return <InputPassword {...prop} />;
    case 'checkbox':
      return <InputCheckbox {...prop} />;
    case 'range':
      return <InputRange {...prop} />;
    case 'radio-button':
      return <RadioButton {...prop} />;
    case 'time-range-picker':
      return <TimeRangePicker {...prop} />;
    case 'date-picker':
      return <DatePicker {...prop} />;
    case 'upload':
      return <Upload {...prop} />;
    case 'upload-image':
      return <UploadImage {...prop} />;
    case 'email':
      return <InputText type={type} {...prop} />;

    default:
      return <InputText {...prop} />;
  }
};