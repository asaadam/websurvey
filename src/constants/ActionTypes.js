// LANGUAGE
export const LANG_SET = 'LANG_SET';

//Contact Module const
export const FETCH_START = 'fetch_start';
export const FETCH_SUCCESS = 'fetch_success';
export const FETCH_ERROR = 'fetch_error';
export const SHOW_MESSAGE = 'SHOW_MESSAGE';
export const HIDE_MESSAGE = 'HIDE_MESSAGE';
export const ON_SHOW_LOADER = 'ON_SHOW_LOADER';
export const ON_HIDE_LOADER = 'ON_HIDE_LOADER';

// AUTH & TOKEN
export const AUTH_SET = 'AUTH_SET';
export const AUTH_SET_TOKEN = 'AUTH_SET_TOKEN';
export const AUTH_CLEAR = 'AUTH_CLEAR';
export const AUTH_SIGNIN_USER = 'AUTH_SIGNIN_USER';
export const AUTH_SIGNIN_USER_SUCCESS = 'AUTH_SIGNIN_USER_SUCCESS';
export const AUTH_SIGNOUT_USER = 'AUTH_SIGNOUT_USER';
export const AUTH_SIGNOUT_USER_SUCCESS = 'AUTH_SIGNOUT_USER_SUCCESS';

// VIEW DIALOG
export const VIEW_SET = 'VIEW_SET';
export const VIEW_OPEN_DIALOG = 'VIEW_OPEN_DIALOG';
export const VIEW_CLOSE_DIALOG = 'VIEW_CLOSE_DIALOG';




