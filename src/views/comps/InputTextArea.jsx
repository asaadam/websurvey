import React, { useState, useEffect } from 'react';
import { Col, Row, Button, Form } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Input Text Area' }
]

const schema = {
    memo1: { text: 'Memo 1', rows: 5 },
    memo2: { text: 'Memo 2' },
    memo3: { text: 'Sample Editor' }
}

const App = ({ match }) => {
    const [form] = Form.useForm();
    const [scope] = useState({ schema });
    const [refs, setRefs] = useState({});

    const editorHandleChange = (value) => {
        console.log({ value });
        // setModel({ ...model, desc: value });
    }

    const setFormula = () => {
        const val = 'e=mc^2+\\sqrt{\\frac {a^2+b^2}{c^2}}+\\int ^{\\infty }_0\\sqrt[x]{\\frac {a^2+b^2}{c^2}+\\sum ^{\\infty }_{n\\mathop{=}0}}';
        console.log(val, refs);

        if (refs.desc) {
            const quill = refs.desc.getEditor();
            quill.setText(val);

            quill.setContents([
                { insert: { formula: val } },
            ]);
        }
    }

    useEffect(() => {
        if (refs.desc) {
            const quill = refs.desc.getEditor();
            const formula = 'e=mc^2+\\sqrt{\\frac {a^2+b^2}{c^2}}+\\int ^{\\infty }_0\\sqrt[x]{\\frac {a^2+b^2}{c^2}+\\sum ^{\\infty }_{n\\mathop{=}0}}';

            quill.setContents([
                { insert: 'Hello', ...{ bold: true } },
                { insert: '\n', ...{ align: 'center' } },
                { insert: { formula } },
                { insert: '\n', ...{ align: 'center' } },
                { insert: 'World', ...{ italic: true } },
                { insert: '\n', ...{ align: 'center' } }
            ]);
        }
    }, [refs.desc]);

    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Form layout="vertical" form={form}>
                    <Row gutter={12}>
                        <Col lg={12}>
                            <InputControl type="textarea" name="memo1" schema={scope.schema} />
                        </Col>
                        <Col lg={12}>
                            <InputControl type="textarea" name="memo2" schema={scope.schema} />
                        </Col>
                    </Row>
                    <Row>
                        <Col >
                            <InputControl type="editor" name="memo3" schema={scope.schema} setRefs={setRefs} onChange={editorHandleChange} />
                        </Col>
                    </Row>
                </Form>
                <hr />
                <Button onClick={setFormula}>Set Value</Button>
            </Content>
        </div>
    );
}

export default App;