import React, { useState, useEffect } from 'react';
import { Col, Row, Form } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';
import oSooka from '../../providers/sooka';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Input Select' }
]

const schema = {
    kecamatan: { name: 'kecamatan', text: 'Kecamatan', type: 'select' },
    kelurahan: { name: 'kelurahan', text: 'Kelurahan', type: 'select' },
}

const App = () => {
    const [form] = Form.useForm();
    const [scope] = useState({ schema });
    const [model, setModel] = useState({ kecamatan: '', kelurahan: '' });
    const [data, setData] = useState({
        kecamatan: [{ value: '', text: '-- Semua Kecamatan --' }],
        kelurahan: [{ value: '', text: '-- Semua Kelurahan --' }]
    });

    const initKecamatan = async () => {
        const list = await oSooka.getDropdown('dropdown-bogor/kecamatan');
        list.unshift({ value: '', text: '-- Semua Kecamatan --' })
        setData({ ...data, kecamatan: list })
    }

    const initKelurahan = async (value) => {
        const url = `dropdown-bogor/kelurahan?kecamatan=${value}`;
        const list = await oSooka.getDropdown(url);
        list.unshift({ value: '', text: '-- Semua Kelurahan --' })
        setData({ ...data, kelurahan: list });
    }

    const onChange = (value, values) => {
        setModel({ ...model, kecamatan: values.value })
    }

    useEffect(() => {
        initKecamatan();
        form.setFieldsValue(model);
    }, [])

    useEffect(() => {
        if (model.kecamatan) {
            setModel({ ...model, kelurahan: '' })
            initKelurahan(model.kecamatan);
            form.setFieldsValue(model);
        }
    }, [model.kecamatan])

    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Form form={form}>
                    <Row gutter={12}>
                        <Col lg={12}>
                            <InputControl name="kecamatan" type="select" schema={scope.schema} data={data.kecamatan} onChange={onChange} />
                        </Col>
                        <Col lg={12}>
                            <InputControl name="kelurahan" type="select" schema={scope.schema} data={data.kelurahan} />
                        </Col>
                    </Row>
                </Form>
            </Content>
        </div>
    );
}

export default App;