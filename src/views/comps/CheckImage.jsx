import React, { useState } from 'react';
import { Col, Row } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Checkbox Image' }
]

const schema = {
    check1: {
        text: 'Kota yang pernah dikunjungi', type: 'checkbox-image', items: [
            { text: 'Bandung', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Jakarta', src: 'https://i.picsum.photos/id/12/200/200.jpg', active: 1 },
            { text: 'Bogor', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Bekasi / Mandiri', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
        ]
    },
    check2: {
        text: 'Kegemaran Anda', type: 'checkbox-image', items: [
            { text: 'Opsi 01', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Opsi 02', src: 'https://i.picsum.photos/id/12/200/200.jpg', active: 1 },
            { text: 'Opsi 03', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Opsi 04', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
        ]
    }
}

const App = () => {
    const [scope] = useState({ schema });

    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Row>
                    <Col ><InputControl {...scope.schema.check1} /></Col>
                </Row>
                <Row>
                    <Col ><InputControl {...scope.schema.check2} /></Col>
                </Row>
            </Content>
        </div>
    );
}

export default App;