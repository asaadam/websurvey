import React, { useState } from 'react';
import { Col, Row, Button, Form } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Input Text' }
]

const initValue = {
    first_name: '',
    last_name: '',
}

const schema = {
    first_name: {
        label: 'Nama Depan',
        rules: [
            { required: true, message: 'Silahkan input Nama depan' },
        ]
    },
    last_name: {
        label: 'Nama Belakang',
        rules: [
            { min: 3, message: 'min 3 characters' },
        ]
    },
    full_name: {
        label: 'Nama Lengkap'
    },
}

const App = ({ match }) => {
    const [scope] = useState({ schema });
    const [form] = Form.useForm();

    const onFinish = (values) => {
        console.log('Success:', values);
    }

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    const onChangeField = (e) => {
        const values = form.getFieldsValue();
        form.setFieldsValue({ ...values, full_name: `${values.first_name || ''} ${values.last_name || ''} ` });
    }


    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Form
                    layout="vertical"
                    form={form}
                    initialValues={initValue}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    <Row gutter={12}>
                        <Col lg={12}><InputControl name="first_name" schema={scope.schema} onChange={onChangeField} /></Col>
                        <Col lg={12}><InputControl name="last_name" schema={scope.schema} onChange={onChangeField} /></Col>
                    </Row>
                    <Row>
                        <Col span={24}><InputControl name="full_name" schema={scope.schema} /></Col>
                    </Row>
                    <hr />

                    <Button htmlType="submit">Submit</Button>
                </Form>
            </Content>
        </div>
    );
}

export default App;
