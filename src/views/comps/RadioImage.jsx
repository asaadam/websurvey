import React, { useState } from 'react';
import { Col, Row } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Radio Image' }
]

const schema = {
    check1: {
        text: 'Agama anda?', type: 'radio-image', items: [
            { text: 'Islam', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Kristen', src: 'https://i.picsum.photos/id/12/200/200.jpg', active: 1 },
            { text: 'Katolik', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Hindu', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Buddha', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
        ]
    },
    check2: {
        text: 'Pendidikan Terakhir', type: 'radio-image', items: [
            { text: 'SD', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'SMP', src: 'https://i.picsum.photos/id/12/200/200.jpg', active: 1 },
            { text: 'SMA', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
            { text: 'Kuliah', src: 'https://i.picsum.photos/id/12/200/200.jpg' },
        ]
    }
}


const App = () => {
    const [scope] = useState({ schema });

    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Row>
                    <Col ><InputControl {...scope.schema.check1} /></Col>
                </Row>
                <Row>
                    <Col ><InputControl {...scope.schema.check2} /></Col>
                </Row>
            </Content>
        </div>
    );
}

export default App;