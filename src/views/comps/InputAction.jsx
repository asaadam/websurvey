import React, { useState } from 'react';
import { Col, Row } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Input Action' }
]


const schema = {
    action1: {
        text: 'Sample Action 1', type: 'action', actions: [
            { text: 'Add', action: 'add', type: 'primary' },
            { text: 'Edit', action: 'edit', style: { margin: '0 5px' } },
            { text: 'Delete', action: 'delete', type: 'danger' }
        ]
    },
    action2: {
        text: 'Sample Action 1', type: 'action', actions: [
            { text: 'Save', action: 'save', type: 'primary' },
            { text: 'Cancel', action: 'cancel', type: 'danger', style: { margin: '0 5px' } }
        ]
    }
}

const App = () => {
    const [scope] = useState({ schema });

    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Row>
                    <Col ><InputControl {...scope.schema.action1} /></Col>
                </Row>
                <Row>
                    <Col ><InputControl {...scope.schema.action2} /></Col>
                </Row>
            </Content>
        </div>
    );
}

export default App;