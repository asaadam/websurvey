import React, { useState } from 'react';
import { Row, Col, Tabs } from 'antd';
import { Content, Breadcrumb, InputControl } from '../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Components' },
    { 'text': 'Tab Panel' }
]

const schema = {
    first_name: {
        label: 'Nama Depan',
        rules: [
            { required: true, message: 'Silahkan input Nama depan' },
        ]
    },
    last_name: {
        label: 'Nama Belakang'
    },
    full_name: {
        label: 'Nama Lengkap'
    },
}

const { TabPane } = Tabs;

const App = () => {
    const [scope, setScope] = useState({ schema });

    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <Tabs defaultActiveKey="1">
                    <TabPane tab="Tab 1" key="1">
                        <Row gutter={12}>
                            <Col lg={12}><InputControl name="first_name" schema={scope.schema} /></Col>
                            <Col lg={12}><InputControl name="last_name" schema={scope.schema} /></Col>
                        </Row>
                        <Row>
                            <Col ><InputControl name="full_name" schema={scope.schema} /></Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Tab 2" disabled key="2">
                        Tab 2
                    </TabPane>
                    <TabPane tab="Tab 3" key="3">
                        Tab 3
                    </TabPane>
                </Tabs>
            </Content>
        </div>
    );
}

export default App;