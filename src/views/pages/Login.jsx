import React, { useState } from 'react';
import { message } from 'antd';
import { connect } from 'react-redux';
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';
import { userSignIn } from '../../stores/actions/auth';

import { useEffect } from 'react';

const App = (props) => {
  const { authUser, showMessage, loader, alertMessage } = props.auth;
  const [model, setModel] = useState({
    email: 'demo@aksaramaya.com',
    password: 'demo@123'
  })

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      props.userSignIn(model);
    } catch (err) {
      console.log(err.message)
    }
  }

  const handleChange = (e) => {
    setModel({ ...model, [e.target.name]: e.target.value });
  }

  useEffect(() => {
    if (authUser !== null) {
      console.log(authUser)
      props.history.push('/');
    }
  }, [authUser])

  return (
    <div className="app flex-row align-items-center">
      <Container>
        <Row className="justify-content-center">
          <Col lg="6">
            <CardGroup>
              <Card className="p-4">
                <CardBody>
                  <Form onSubmit={handleSubmit}>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Username" autoComplete="email" onChange={handleChange} name="email" value={model.email} />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Password" autoComplete="current-password" onChange={handleChange} name="password" value={model.password} />
                    </InputGroup>
                    <Row>
                      <Col xs="6">
                        <Button color="primary" onSubmit={handleSubmit} className="px-4">Login</Button>
                      </Col>
                      <Col xs="6" className="text-right">
                        <Button color="link" className="px-0">Forgot password?</Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </CardGroup>
          </Col>
        </Row>
        {loader ? (<div className="gx-loader-view">loading...</div>) : null}
        {showMessage ? message.error(alertMessage.toString()) : null}
      </Container>
    </div>
  );
}

const mapStateToProps = ({ auth }) => ({ auth });
export default connect(mapStateToProps, { userSignIn })(App);
