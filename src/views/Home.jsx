import React, { useEffect, useState } from "react";
import i18next from "i18next";
import Content from "../components/Content";
import { connect } from "react-redux";
import { Form, Input, Button } from "antd";
import InputControl from "../components/InputControl";
import oApi from "../providers/webhook";

const App = ({ match }) => {
  // const doRefreshToken = async () => {
  //   await oAuth.refreshToken();
  // };
  let form;
  const [dataForm, setDataForm] = useState();
  const [surveyInfo, setSurveyInfo] = useState();
  const [schema, setSchema] = useState();
  const [surveyData, setSurveyData] = useState();
  const [password, setPassword] = useState(false);
  const [wrong, setWrong] = useState(false);
  const [input, setInput] = useState("");
  const [token, setToken] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [already, setAlready] = useState(false);
  let survey_id = match.params.id;

  const getQuestion = async () => {
    let base_url = `anonymous_survey_question?survey_id=${survey_id}&token=${token}`;
    if (token) {
      const { data } = await oApi.list(base_url);
      data.sort((a, b) => (a.order > b.order ? 1 : b.order > a.order ? -1 : 0));
      const info = await oApi.list(
        `associatesurveyor_survey_info?survey_id=${survey_id}`
      );
      setSurveyInfo(info.data);
      setDataForm(data);
    }
  };

  const getData = async () => {
    if (localStorage.getItem(survey_id)) {
      setAlready(true);
    }
    const result = await oApi.list(`anonymous_survey?survey_id=${survey_id}`);
    setSurveyData(result.data);
    if (result.data.need_password) {
      setPassword(true);
      return;
    }
    setToken(result.data.token);
    getQuestion();
  };

  const onSubmit = async (data) => {
    setIsLoading(true);
    let base_url = `anonymous_survey_answer?survey_id=${survey_id}&token=${token}`;
    try {
      const body = {
        data: {
          attributes: {
            survey_id,
            ...data,
          },
        },
      };
      console.log(body);
      await oApi.insert(base_url, body);
      localStorage.setItem(survey_id, survey_id);
      setAlready(true);
    } catch (e) {
      console.log(e);
    }
    setIsLoading(false);
  };

  const checkPassword = async () => {
    const data = {
      data: {
        password: input,
      },
    };
    try {
      const result = await oApi.insert(
        `anonymous_survey_check_password?survey_id=${survey_id}`,
        data
      );
      setToken(result.data.token);
      setPassword(false);
    } catch (e) {
      setWrong(true);
    }
  };

  const generateSchema = () => {
    let schema = {};
    dataForm.map(
      (data) =>
        (schema[data.key] = {
          label: data.label,
        })
    );

    setSchema(schema);
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (token) {
      getQuestion();
    }
  }, [token]);

  useEffect(() => {
    if (dataForm) {
      generateSchema();
    }
  }, [dataForm]);

  const generateForm = (data) => {
    let options = [];
    if (data.reference.key) {
      surveyInfo.forEach((value) => {
        if (data.reference.key === value.key) {
          value.data.data.forEach((option) => {
            options.push({
              value: option.value,
              text: option.name,
            });
          });
        }
      });
    } else if (data.reference.views) {
      data.reference.views.forEach((value) => {
        options.push({
          value: value.value,
          text: value.name,
        });
      });
    }

    switch (data.survey_question_type.name) {
      case "Text Box":
        return (
          <InputControl name={data.key} schema={schema} extra={data.hint} />
        );
      case "Number Text Box":
        return (
          <InputControl
            name={data.key}
            schema={schema}
            type="number"
            extra={data.hint}
          />
        );
      case "Date":
        return (
          <InputControl
            name={data.key}
            type={"date-picker"}
            schema={schema}
            extra={data.hint}
          />
        );
      case "Drop Down":
        return (
          <InputControl
            name={data.key}
            type="select"
            schema={schema}
            data={options}
            extra={data.hint}
          />
        );
      case "Multi Select":
        return (
          <InputControl
            name={data.key}
            type="select"
            schema={schema}
            data={options}
            mode="multiple"
            extra={data.hint}
          />
        );
      case "Check Box":
        return (
          <InputControl
            name={data.key}
            type={"checkbox"}
            schema={schema}
            extra={data.hint}
          />
        );
      default:
        return <Form.Item label={data.label}></Form.Item>;
    }
  };
  const layoutCols = {
    labelCol: { span: 8 },
    wrapperCol: { span: 10 },
  };

  return (
    <div className="mt-4">
      <Content
        title={
          surveyData ? (
            <div>
              <p style={{ marginBottom: 0 }}>{surveyData.name}</p>
              <span
                style={{
                  fontSize: "0.9rem",
                  marginTop: 8,
                  fontWeight: "normal",
                }}
              >
                {surveyData.description}
              </span>
            </div>
          ) : (
            "Mengambil Data..."
          )
        }
      >
        {already ? (
          <h3>Anda sudah Mengisi Survey ini</h3>
        ) : password ? (
          <>
            <span>Masukkan Password</span>
            <Input.Password
              placeholder="input password"
              onChange={(data) => setInput(data.target.value)}
            />
            {wrong && (
              <p style={{ color: "red" }}>Password yang anda masukkan salah</p>
            )}

            <Button onClick={checkPassword}>Submit</Button>
          </>
        ) : dataForm ? (
          <Form {...layoutCols} form={form} onFinish={onSubmit}>
            {dataForm.map((data) => generateForm(data))}
            <Button
              style={{ width: "100px" }}
              className="ml-1"
              type="primary"
              htmlType="submit"
              loading={isLoading}
            >
              Simpan
            </Button>
          </Form>
        ) : null}
      </Content>
    </div>
  );
};

const mapState = ({ auth, lang }) => ({ auth: auth.authUser, lang });

export default connect(mapState)(App);
