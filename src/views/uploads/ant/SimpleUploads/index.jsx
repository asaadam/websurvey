import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import { Upload, message, Button } from 'antd';
import Icon from '@ant-design/icons';

import config from './index.config';
import Content from '../../../../components/Content';
import Breadcrumb from '../../../../components/Breadcrumb';

const App = () => {
    const { breadcrumb, content, methods } = config;
    const [list, setList] = useState([]);

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content}>
                <Row>
                    <Col lg="6">
                        <Upload
                            action={config.fields.upload.urlAction}
                            onChange={(e) => methods.onUpload(e, message, setList)}
                        >
                            <Button>
                                <Icon type={config.fields.upload.icon} />
                                {config.fields.upload.text}
                            </Button>
                        </Upload>
                    </Col>
                    <Col lg="6">
                        <pre>
                            {JSON.stringify(list, null, 4)}
                        </pre>
                    </Col>
                </Row>
            </Content>
        </React.Fragment>
    );
}

export default App;
