const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

export default {
    breadcrumb: [
        { text: 'Home', to: '/' },
        { text: 'Ant Upload', to: '/upload/ant' },
        { text: 'Gallery Uploads' },
    ],
    content: {
        title: 'Gallery Uploads',
    },
    fields: {
        upload: {
            icon: 'upload',
            text: 'Click to Upload',
            urlAction: 'https://www.mocky.io/v2/5185415ba171ea3a00704eed'
        }
    },
    methods: {
        onUpload: async (e, message, setList) => {
            message.config({
                top: 110,
                duration: 2,
            });

            if (e.file.status !== 'uploading') {
                setList(e.fileList);
            }

            if (e.file.status === 'done') {
                message.success(`${e.file.name} file uploaded successfully`);
            } else if (e.file.status === 'error') {
                message.error(`${e.file.name} file upload failed.`);
            }
        },
        handlePreview: async (file, setState) => {
            if (!file.url && !file.preview) {
                file.preview = await getBase64(file.originFileObj);
            }

            setState({
                previewImage: file.url || file.preview,
                previewVisible: true,
            });
        },
        handleChange: ({ fileList }, setState) => {
            setState({ fileList });
        },
        handleCancel: (setState) => {
            setState({ previewVisible: false });
        }
    }
};