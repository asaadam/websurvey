import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import { Upload, Modal } from 'antd';
import Icon from '@ant-design/icons';

import config from './index.config';

import Content from '../../../../components/Content';
import Breadcrumb from '../../../../components/Breadcrumb';

const App = () => {
    const { breadcrumb, content, methods } = config;
    const [state, setState] = useState({
        previewVisible: false,
        previewImage: '',
        fileList: [
            {
                uid: '-1',
                name: 'image.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            },
            {
                uid: '-2',
                name: 'image.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            },
            {
                uid: '-3',
                name: 'image.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            },
            {
                uid: '-4',
                name: 'image.png',
                status: 'done',
                url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
            },
            {
                uid: '-5',
                name: 'image.png',
                status: 'error',
            },
        ],
    });

    const uploadButton = (
        <div>
            <Icon type="plus" />
            <div className="ant-upload-text">Upload</div>
        </div>
    );

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content}>
                <Row>
                    <Col lg="6">
                        <Upload
                            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                            listType="picture-card"
                            fileList={state.fileList}
                            onPreview={(e) => methods.handlePreview(e, setState)}
                            onChange={(e) => methods.handleChange(e, setState)}
                        >
                            {state.fileList.length >= 8 ? null : uploadButton}
                        </Upload>
                        <Modal visible={state.previewVisible} footer={null} onCancel={() => methods.handleCancel(setState)}>
                            <img alt="example" style={{ width: '100%' }} src={state.previewImage} />
                        </Modal>
                    </Col>
                </Row>
            </Content>
        </React.Fragment>
    );
}

export default App;
