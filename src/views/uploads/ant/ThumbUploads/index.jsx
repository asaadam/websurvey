import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import { Upload, message, Button } from 'antd';
import Icon from '@ant-design/icons';
import config from './index.config';

import Content from '../../../../components/Content';
import Breadcrumb from '../../../../components/Breadcrumb';

message.config({ top: 110, duration: 2 });

const App = () => {
    const { breadcrumb, content, methods } = config;
    const [list, setList] = useState([]);
    const [state, setState] = useState({ imageUrl: '', loading: false });

    const uploadButton = (
        <div style={{ width: 200, padding: '40px 0' }}>
            <Icon type={state.loading ? 'loading' : 'plus'} />
            <div className="ant-upload-text">Upload</div>
        </div>
    );

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content}>
                <Row>
                    <Col lg="6">
                        <Upload
                            action={config.fields.upload.urlAction}
                            onChange={(e) => methods.onUpload(e, message, setList)}
                            listType="picture"
                        >
                            <Button>
                                <Icon type={config.fields.upload.icon} />
                                {config.fields.upload.text}
                            </Button>
                        </Upload>
                        <br />
                        <br />
                        <br />
                        <Upload
                            listType="picture-card"
                            showUploadList={false}
                            action={config.fields.upload2.urlAction}
                            onChange={(e) => methods.handleUpload(e, setState)}
                            beforeUpload={(e) => methods.beforeUpload(e, message)}
                        >
                            {state.imageUrl ? <img src={state.imageUrl} alt="" style={{ width: '200px' }} /> : uploadButton}
                        </Upload>
                    </Col>
                    <Col lg="6">
                        <pre>
                            {JSON.stringify(list, null, 4)}
                        </pre>
                    </Col>
                </Row>
            </Content>
        </React.Fragment>
    );
}

export default App;
