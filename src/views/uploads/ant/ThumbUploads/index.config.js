const getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

export default {
    breadcrumb: [
        { text: 'Home', to: '/' },
        { text: 'Ant Upload', to: '/upload/ant' },
        { text: 'Thumbnail Uploads' },
    ],
    content: {
        title: 'Thumbnail Uploads',
    },
    fields: {
        upload: {
            icon: 'upload',
            text: 'Click to Upload',
            urlAction: 'https://www.mocky.io/v2/5185415ba171ea3a00704eed'
        },
        upload2: {
            icon: 'upload',
            text: 'Click to Upload',
            urlActiox: 'https://www.mocky.io/v2/5185415ba171ea3a00704eed',
            urlAction: 'https://www.mocky.io/v2/5185415ba171ea3a00704eed'
        }
    },
    methods: {
        onUpload: (e, message, setList) => {
            if (e.file.status !== 'uploading') {
                setList(e.fileList);
            }

            if (e.file.status === 'done') {
                message.success(`${e.file.name} file uploaded successfully`);
            } else if (e.file.status === 'error') {
                message.error(`${e.file.name} file upload failed.`);
            }
        },
        beforeUpload: (file, message) => {
            const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
            if (!isJpgOrPng) {
                message.error('You can only upload JPG/PNG file!');
            }
            const isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
                message.error('Image must smaller than 2MB!');
            }
            return isJpgOrPng && isLt2M;
        },
        handleUpload: (e, setState) => {
            if (e.file.status === 'uploading') {
                setState({ loading: true });
                return;
            }

            if (e.file.status === 'done') {
                // Get this url from response in real world.
                getBase64(e.file.originFileObj, imageUrl =>
                    setState({ imageUrl, loading: false, }),
                );
            }
        }
    }
};