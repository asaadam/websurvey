import React, { useState, useEffect } from 'react';
import config from './index.config';
import { Table, message } from 'antd';

import Content from '../../../../components/Content';
import Breadcrumb from '../../../../components/Breadcrumb';
import oProvider from '../../../../providers/webhook';

message.config({ top: 110, duration: 2 });

const App = () => {
    const { breadcrumb, content } = config;
    const [state, setState] = useState(config.state);

    const refresh = async () => {
        const { current, pageSize } = state.pagination;
        const url = `holding?page=${current}&limit=${pageSize}`;

        setState((data) => ({ ...data, loading: true }));

        const { meta, data } = await oProvider.list(url);

        setState((state) => ({
            ...state,
            loading: false,
            pagination: { ...state.pagination, total: meta.total },
            data
        }));
    }
    const toolbars = [
        {
            text: 'Add', type: 'primary', icon: 'plus',
            onClick: () => {
                console.log('-- Add --')
                message.success(`-- action add --`);
            }
        },
        {
            text: 'Refresh', type: 'success', icon: 'reload',
            onClick: () => {
                refresh();
            }
        },
        {
            text: 'Delete', type: 'danger', icon: 'delete',
            onClick: () => {
                console.log('-- Delete --')
                message.warning(`-- action delete --`);
            }
        },
    ]
    const handleTableChange = (pagination) => {
        setState((state) => ({ ...state, pagination }));
    }

    useEffect(() => {
        refresh()
    }, [state.pagination.current])

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content} toolbars={toolbars}>
                <Table
                    rowKey={row => row.id}
                    rowSelection={config.rowSelection}
                    columns={config.columns}
                    dataSource={state.data}
                    loading={state.loading}
                    pagination={state.pagination}
                    onChange={handleTableChange}
                />
            </Content>
        </React.Fragment>
    );
}

export default App;
