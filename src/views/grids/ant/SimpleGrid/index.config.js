import React from 'react';
import { Button } from 'antd';

export default {
    state: {
        loading: false,
        pagination: {
            current: 1,
            pageSize: 5,
            total: 10,
            showLessItems: true,
        },
        filter: {},
        data: []
    },
    breadcrumb: [
        { text: 'Home', to: '/' },
        { text: 'Ant Grid', to: '/grid/ant' },
        { text: 'Simple Grid' },
    ],
    content: {
        title: 'List of Organization Group',
    },
    columns: [
        {
            title: 'Group Name',
            dataIndex: 'organization_group_name',
        },
        {
            title: 'Email',
            dataIndex: 'organization_group_email',
        },
        {
            title: 'Active?',
            dataIndex: 'organization_group_isactive',
            className: 'text-center',
            render: (value) => {
                return value ? 'Ya' : '-';
            },
        },
        {
            title: 'Action',
            key: 'operation',
            fixed: 'right',
            className: 'text-center',
            width: 180,
            render: () => (
                <div>
                    <Button type="link">Edit</Button>|<Button type="link">Delete</Button>
                </div>
            ),
        },
    ],
    rowSelection: {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: record => ({
            disabled: record.name === 'Disabled User', // Column configuration not to be checked
            name: record.name,
        }),
    }
};