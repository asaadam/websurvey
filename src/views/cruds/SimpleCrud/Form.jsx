import React from 'react';
import { Button, Form, Row, Col } from 'antd';

import InputText from '../../../components/ant/InputText';

const Edit = (props) => {
  const { onSubmit, onCancel, schema } = props;
  const defCol = { lg: { span: 16 }, xl: { span: 14 } }

  return (
    <React.Fragment>
      <Form onSubmit={onSubmit}>
        <Row>
          <Col {...defCol}>
            <InputText name="category_name" schema={schema} />
          </Col>
        </Row>
        <hr />
        <Button htmlType="submit">Save</Button>
        <Button className="ml-1" onClick={onCancel}>Cancel</Button>
      </Form>
    </React.Fragment>
  );
}

export default Edit;