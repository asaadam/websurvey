import React from 'react';
import { Form } from 'antd';

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';
import FormControl from './Form';

import oRest from '../../../providers/webhook';
import config from './index.config';

const App = (props) => {
  const { schema } = config;
  const { breadcrumb, content } = config.add;

  const backToList = () => {
    props.history.push('/crud/simple');
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.form.validateFields(async (errors, values) => {
      if (!errors) {
        try {
          await oRest.insert(`category`, values)
          backToList();
        } catch (err) {
          console.log(err);
        }
      }
    })
  }

  schema.form = props.form;

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
        />
      </Content>
    </React.Fragment>
  );
}

export default Form.create({ name: 'organization_add_form' })(App);