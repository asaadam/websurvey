import React from 'react';
import { Button } from 'antd';

export default {
  list: {
    state: {
      loading: false,
      pagination: {
        current: 1,
        pageSize: 5,
        total: 10,
        showLessItems: true,
      },
      selectedRowKeys: [],
      filter: {},
      data: []
    },
    breadcrumb: [
      { text: 'Home', to: '/' },
      { text: 'Master', to: '/crud' },
      { text: 'List of Category' },
    ],
    content: {
      title: 'List of Category',
      toolbars: [
        {
          text: 'Add', type: 'primary', icon: 'plus',
          onClick: (e) => {
            e.history.push('/crud/simple/add');
          }
        },
      ]
    },
    configGrid: ({ onEdit, onDelete }) => {
      return {
        columns: [
          {
            title: 'Nama Kategori',
            dataIndex: 'category_name',
            sorter: true
          },
          {
            title: 'Action',
            width: 180,
            className: 'text-center',
            render: (row) => (
              <div>
                <Button type="link" onClick={() => onEdit(row)}>Edit</Button>
                <span>|</span>
                <Button type="link" onClick={() => onDelete(row)}>Delete</Button>
              </div>
            ),
          },
        ]
      }
    }
  },
  add: {
    breadcrumb: [
      { text: 'Home', to: '/' },
      { text: 'Master', to: '/crud' },
      { text: 'List of Category', to: '/crud/simple' },
      { text: 'Add Category' },
    ],
    content: {
      title: 'Add Category',
    },
  },
  edit: {
    breadcrumb: [
      { text: 'Home', to: '/' },
      { text: 'Master', to: '/crud' },
      { text: 'List of Category', to: '/crud/simple' },
      { text: 'Edit Category' },
    ],
    content: {
      title: 'Edit Category',
    },
  },
  schema: {
    category_name: {
      label: 'Nama Kategori',
      rules: [
        { required: true, message: 'Silahkan input kode kategori' },
      ]
    },
  }
};