import React from 'react';
import ListForm from './FormList';

const App = (props) => {
  return (<ListForm {...props} />);
}

export default App;
