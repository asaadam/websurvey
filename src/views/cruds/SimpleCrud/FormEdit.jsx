import React, { useEffect } from 'react';
import { Form } from 'antd';

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';
import FormControl from './Form';

import oApi from '../../../providers/webhook';
import config from './index.config';

const App = ({ match, history, form }) => {
  const { schema } = config;
  const { breadcrumb, content } = config.edit;
  const id = match.params.id;

  const backToList = () => {
    history.push('/crud/simple');
  };

  const getDetail = async () => {
    const { data } = await oApi.list(`category?id=${id}`);
    form.setFieldsValue({ category_name: data[0].category_name });
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    form.validateFields(async (errors, values) => {
      if (!errors) {
        try {
          await oApi.update(`category?id=${id}`, values)
          backToList();
        } catch (err) {
          console.log(err);
        }
      }
    })
  }

  useEffect(() => {
    getDetail();
  }, []);

  schema.form = form;

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <FormControl
          onSubmit={handleSubmit}
          onCancel={backToList}
          schema={schema}
        />
      </Content>
    </React.Fragment>
  );
}

export default Form.create({ name: 'organization_edit_form' })(App);