import React, { useEffect, useState } from 'react';
import { Modal, notification } from 'antd';
import i18next from 'i18next';

import Content from '../../../components/Content';
import MocoTable from '../../../components/MocoTable';
import Breadcrumb from '../../../components/Breadcrumb';

import oApi from '../../../providers/webhook';
import configCrud from './index.config';

const { list: config } = configCrud;
const { confirm } = Modal;

const List = ({ history }) => {
  const { breadcrumb, content } = config;
  const [state, setState] = useState(config.state);

  const refresh = async () => {
    const { current, pageSize } = state.pagination;
    let url = `category?page=${current}&limit=${pageSize}`;

    setState((data) => ({ ...data, loading: true, selectedRowKeys: [] }));

    if (state.sorter) {
      if (state.sorter.field) url += `&sortBy=${state.sorter.field}`;
      if (state.sorter.order) url += `&sortDir=${state.sorter.order === 'ascend' ? 'asc' : 'desc'} `;
    }

    const { meta, data } = await oApi.list(url);

    setState((state) => ({
      ...state,
      loading: false,
      pagination: { ...state.pagination, total: meta.total },
      data
    }));
  }

  const handleTableChange = (pagination, filter, sorter) => {
    setState((state) => ({ ...state, pagination, filter, sorter }));
  }

  const rowSelection = {
    selectedRowKeys: state.selectedRowKeys,
    onChange: (selectedRowKeys, selectedRows) => {
      setState((state) => ({ ...state, selectedRowKeys }));
    },
    getCheckboxProps: record => ({
      disabled: record.name === 'Disabled User', // Column configuration not to be checked
      name: record.name,
    }),
  }

  const onAdd = () => {
    history.push('/crud/simple/add');
  }

  const onEdit = (e) => {
    history.push(`/crud/simple/${e.id}`);
  }

  const onDelete = (e) => {
    confirm({
      title: i18next.t('message.delete_content'),
      content: i18next.t('message.delete_content_info'),
      onOk() {
        return new Promise(async (resolve, reject) => {
          try {
            await oApi.delete(`category?id=${e.id}`)
            resolve();
            refresh();

            notification.success({
              message: 'Delete Success',
              description: 'Data berhasil dihapus',
              placement: 'bottomRight'
            });
          } catch (err) {
            resolve();

            notification.error({
              message: 'Delete Error',
              description: err.message,
              placement: 'bottomRight'
            });
          }
        });
      },
      onCancel() { },
    });
  }

  const onDeleteBatch = () => {
    confirm({
      title: i18next.t('message.delete_content'),
      content: i18next.t('message.delete_content_info'),
      onOk() {
        return new Promise(async (resolve, reject) => {
          const { selectedRowKeys } = state;
          try {
            for (var idx in selectedRowKeys) {
              await oApi.delete(`category?id=${selectedRowKeys[idx]}`);
            };
            resolve();
            refresh();

            notification.success({
              message: 'Delete Success',
              description: 'Data berhasil dihapus',
              placement: 'bottomRight'
            });
          } catch (err) {
            reject(err);

            notification.error({
              message: 'Delete Error',
              description: err.message,
              placement: 'bottomRight'
            });
          }
        });
      },
      onCancel() { },
    });
  }

  const toolbars = [
    { text: 'Add', type: 'primary', icon: 'plus', onClick: onAdd },
    { text: 'Refresh', type: 'success', icon: 'reload', onClick: refresh },
    { text: 'Delete', type: 'danger', icon: 'delete', onClick: onDeleteBatch, active: state.selectedRowKeys.length },
  ]

  useEffect(() => {
    refresh()
  }, [state.pagination.current, state.sorter])

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content} toolbars={toolbars.filter((m, idx) => idx >= 2 ? m.active : true)}>
        <MocoTable
          {...config.configGrid({ onEdit, onDelete })}
          {...state}
          rowSelection={rowSelection}
          onChange={handleTableChange}
          dataSource={state.data}
        />
      </Content>
    </React.Fragment >
  );
}

export default List;