export default {
  breadcrumb: [
    { text: 'Home', to: '/' },
    { text: 'Editor', to: '/editor' },
    { text: 'More Toolbar Quill' },
  ],
  content: {
    title: 'More Toolbar Quill',
  },
  quill: {
    modules: {
      toolbar: [
        ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
        ['blockquote', 'code-block'],

        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction

        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],

        ['clean']
      ]
    },
  },
  model: {
    text: `
    <h1 class="ql-align-center">Quill Rich Text Editor</h1>
    <p><br></p>
    <p>Quill is a free, <a href="https://github.com/quilljs/quill/" rel="noopener noreferrer" target="_blank">open source</a> 
    WYSIWYG editor built for the modern web. With its <a href="https://quilljs.com/docs/modules/" rel="noopener noreferrer" target="_blank">modular architecture</a> and expressive <a href="https://quilljs.com/docs/api/" rel="noopener noreferrer" target="_blank">API</a>, 
    it is completely customizable to fit any need.</p>
    
    <ul>
      <li>Item 001</li>
      <li>Item 002</li>
      <li>Item 003</li>
    </ul>
    <p><br></p>`
  }
};