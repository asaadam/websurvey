import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';
import ReactQuill from 'react-quill'

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';

import config from './index.config';

const App = () => {
  const { breadcrumb, content } = config;
  const [state, setState] = useState({ text: config.model.text });

  const handleChange = (value) => {
    setState({ text: value })
  }

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <Row>
          <Col lg="6">
            <ReactQuill value={state.text} onChange={handleChange} />
          </Col>
          <Col lg="6">
            <span dangerouslySetInnerHTML={{ __html: state.text }} />
          </Col>
        </Row>
      </Content>
    </React.Fragment>
  );
}

export default App;
