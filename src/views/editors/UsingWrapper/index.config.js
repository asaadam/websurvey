export default {
  breadcrumb: [
    { text: 'Home', to: '/' },
    { text: 'Editor', to: '/editor' },
    { text: 'Using Wrapper' },
  ],
  content: {
    title: 'Using Wrapper',
  },
  model: {
    text: `
    <h1 class="ql-align-center">Quill Rich Text Editor</h1>
    <p><br></p>
    <p>Quill is a free, <a href="https://github.com/quilljs/quill/" rel="noopener noreferrer" target="_blank">open source</a> 
    WYSIWYG editor built for the modern web. With its <a href="https://quilljs.com/docs/modules/" rel="noopener noreferrer" target="_blank">modular architecture</a> and expressive <a href="https://quilljs.com/docs/api/" rel="noopener noreferrer" target="_blank">API</a>, 
    it is completely customizable to fit any need.</p>
    
    <ul>
      <li>Item 001</li>
      <li>Item 002</li>
      <li>Item 003</li>
    </ul>
    <p><br></p>`
  }
};