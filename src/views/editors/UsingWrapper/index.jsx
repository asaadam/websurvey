import React, { useState } from 'react';
import { Col, Row } from 'reactstrap';

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';
import MocoEditor from '../../../components/MocoEditor';

import config from './index.config';

const App = () => {
  const { breadcrumb, content } = config;
  const [state, setState] = useState({ text: config.model.text });

  const handleChange = (value) => {
    setState({ text: value })
  }

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumb} />
      <Content {...content}>
        <Row>
          <Col lg="12">
            <MocoEditor value={state.text} onChange={handleChange} />
          </Col>
          <Col lg="12">
            <div className="mt-5"></div>
            <MocoEditor value={state.text} display={true} />
          </Col>
        </Row>
      </Content>
    </React.Fragment>
  );
}

export default App;
