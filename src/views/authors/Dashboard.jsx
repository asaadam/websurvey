import React, { useState } from 'react';
import Breadcrumb from '../../components/Breadcrumb';
import { Card, Row, Col, Button, Badge } from 'antd';
import './Dashboard.scss';

const Card0 = ({ title }) => {
    return (
        <Col span={12} style={{ marginBottom: 20 }}>
            <Card title={title}>
                <div>Current Working Script</div>
            </Card>
        </Col>
    )
};
const Card1 = (props) => {
    return (
        <Col span={12} style={{ marginBottom: 20 }}>
            <Card title={props.title}
            >
                <Row>
                    <Col span={10}>
                        <img width="100%" src={props.image} alt={props.image} />
                    </Col>
                    <Col span={14}>
                        {props.list.map((item, idx) => (
                            <div key={idx} className="list-box">
                                <div className="list-label">{item.text}</div>
                                <div className="list-value">{item.value}</div>
                            </div>
                        ))}
                        <div style={{ marginLeft: 20, marginTop: 50 }}>
                            <Button type="primary">{props.action.text}</Button>
                        </div>
                    </Col>
                </Row>
            </Card>
        </Col>
    )
};
const Card2 = (props) => {
    return (
        <Col span={12} style={{ marginBottom: 20 }}>
            <Card title={(
                <div>
                    <span>{props.title}</span>
                    <Badge count={props.badge} style={{ marginLeft: 20 }} />
                </div>
            )}>
                <div>
                    {props.list.map((item, idx) => (
                        <Row key={idx} className="list-box" style={{ marginBottom: 18 }}>
                            <Col span={10} className="list-label">{item.name}</Col>
                            <Col span={14}>{item.text}</Col>
                        </Row>
                    ))}
                </div>
            </Card>
        </Col>
    )
};
const CardBox = (props) => {
    switch (props.type) {
        case 'card1':
            return <Card1 {...props} />
        case 'card2':
            return <Card2 {...props} />
        default:
            return <Card0  {...props} />
    }
}

const App = () => {
    const [scope] = useState(
        {
            breadcrumbs: [
                { text: 'Home', to: '/' },
                { text: 'Authoring Tools' },
                { text: 'Dashboard' }
            ]
        }
    );
    const [state] = useState({
        data: [
            {
                title: 'Current Working Script',
                type: 'card1',
                image: 'https://i.picsum.photos/id/2/400/500.jpg',
                list: [
                    { text: 'Start working date', value: '19 April 2020 10:30' },
                    { text: 'Last working date', value: '25 April 2020 17:30' },
                ],
                action: { text: 'Continue Working' }
            },
            {
                title: 'My Inbox',
                type: 'card2',
                badge: 5,
                list: [
                    { name: 'Luffy (publisher)', text: 'Current progress on yout book' },
                    { name: 'Zorro (layouter)', text: 'front page working' },
                    { name: 'Aladin (layouter)', text: 'front page template' },
                    { name: 'Nami (publisher)', text: 'your book published' },
                    { name: 'Sanji (publisher)', text: 'please revise' },
                ]
            },
            {
                title: 'My Book'
            },
            {
                title: 'My Team & Support'
            }
        ]
    })

    return (
        <React.Fragment>
            <Breadcrumb items={scope.breadcrumbs} />
            <div className="authors-tools">
                <Row gutter={12}>
                    {state.data.map((item, idx) => (<CardBox {...item} key={idx} />))}
                </Row>
            </div>
        </React.Fragment >
    );
}

export default App;