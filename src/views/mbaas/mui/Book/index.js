import React from 'react';
import Breadcrumb from '../../../../components/Breadcrumb';

const App = () => {
  const breadcrumbs = [
    { text: 'Home', to: '/' },
    { text: 'MBaas Service' },
    { text: 'List of Book' },
  ];

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumbs} />
    </React.Fragment>
  );
}

export default App;