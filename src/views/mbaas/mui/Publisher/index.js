import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PublisherEdit from './PublisherEdit';
import PublisherList from './PublisherList';
import Breadcrumb from '../../../../components/Breadcrumb';
import oBaas from '../../../../providers/baas';
import ConfirmDialog from '../../../../components/mui/ConfirmDialog';
import * as ActionType from '../../../../redux/view/contant';

const initScope = {
  state: 'list',
  model: { fields: '' }
}

const App = (props) => {
  const breadcrumbs = [
    { text: 'Home', to: '/' },
    { text: 'MBaas Service' },
    { text: 'List of Publisher' },
  ];
  const [scope, setScope] = useState(initScope);
  const [list, setList] = useState([]);

  const refreshGrid = async () => {
    const resp = await oBaas.list('publisher');
    setList(resp.data)
    console.log('-- refresh grid --');
  };
  const changeState = (state, model) => {
    setScope({ ...scope, state, model })
  };
  const onSave = async (row) => {
    if (row.id) {
      await oBaas.update(`publisher/${row.id}`, row.fields);
    } else {
      await oBaas.insert(`publisher`, row.fields);
    }
    scope.state = initScope.state;
    scope.model = initScope.model;
    refreshGrid();
  };
  const onCancel = () => {
    changeState(initScope.state, initScope.model);
  };
  const onDelete = () => {
    props.open();
  };

  useEffect(() => {
    refreshGrid();
  }, []);

  return (
    <React.Fragment>
      <Breadcrumb items={breadcrumbs} />
      <div style={{ padding: '0 30px' }}>
        <PublisherList changeState={changeState} onDelete={onDelete} data={list} />
      </div>
      <br />
      <PublisherEdit open={scope.state !== 'list'} scope={scope} setScope={setScope} onSave={onSave} onCancel={onCancel} />
      <ConfirmDialog />
    </React.Fragment>
  )
}

const mapDispatch = (dispatch) => {
  return {
    openDialog: () => dispatch(ActionType.VIEW_OPEN_DIALOG),
    closeDialog: () => dispatch(ActionType.VIEW_OPEN_DIALOG)
  }
}

export default connect(null, mapDispatch)(App);
