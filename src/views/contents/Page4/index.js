import React from 'react';
import { Content, Breadcrumb } from '../../../components';

const items = [
    { 'text': 'Home', 'to': '/' },
    { 'text': 'Layout' },
    { 'text': 'Content without Header' }
]

const App = () => {
    return (
        <div>
            <Breadcrumb items={items} />
            <Content>
                <p>Here sample content without header</p>
            </Content>
        </div>
    );
}

export default App;