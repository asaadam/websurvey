import React, { useEffect, useState } from 'react';
import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';

import oData from '../../../providers/firebase';

const App = ({ match }) => {
  const [scope, setScope] = useState({ breadcrumbs: [{ text: 'Home', to: '/' }, { text: 'Layout' }] });

  useEffect(() => {
    oData.getConfigRef(match.url).on('value', (snapshot) => {
      const values = snapshot.val();
      setScope(values)
    });

    return () => {
      oData.getConfigRef(match.url).off('value');
    }
  }, [])

  // oData.setConfig(match.url, scope);

  return (
    <React.Fragment>
      <Breadcrumb items={scope.breadcrumbs} />
      <Content {...scope}>
        <p>Hello Page 2</p>
      </Content >
    </React.Fragment>
  );
}

export default App;
