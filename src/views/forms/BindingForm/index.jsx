import React from 'react';
import { useState } from 'react';
import { Col, Row } from 'antd';
import config from './index.config';

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';
import InputText from '../../../components/ant/InputText';
import InputNumber from '../../../components/ant/InputNumber';
import InputTextArea from '../../../components/ant/InputTextArea';
import InputSelect from '../../../components/ant/InputSelect';

const App = () => {
    const { breadcrumb, content } = config;
    const [model, setModel] = useState({});

    const handleChange = (name, value) => {
        setModel({ ...model, [name]: value });
    }

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content}>
                <Row gutter={40}>
                    <Col xl={16}>
                        <Row gutter={12}>
                            <Col xl={12}>
                                <InputText text="First Name" name="first_name" onChange={handleChange} />
                            </Col>
                            <Col xl={12}>
                                <InputText text="Last Name" name="last_name" onChange={handleChange} />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl={24}>
                                <InputText text="Full Name" value={`${model.first_name || ''} ${model.last_name || ''}`} disabled />
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col xl={12}>
                                <InputText text="Phone No" name="phone_no" onChange={handleChange} />
                            </Col>
                            <Col xl={12}>
                                <InputText text="Email" name="email" onChange={handleChange} />
                            </Col>
                        </Row>
                        <Row gutter={12}>
                            <Col xl={12}>
                                <InputNumber text="Gross Amount" name="amount1" onChange={handleChange} />
                            </Col>
                            <Col xl={12}>
                                <InputNumber text="Net Amount" name="amount2" onChange={handleChange} />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl={24}>
                                <InputSelect text="Company" data={config.data.company} name="company" onChange={handleChange} />
                            </Col>
                        </Row>
                        <Row>
                            <Col xl={24}>
                                <InputTextArea text="Address" name="address" onChange={handleChange} />
                            </Col>
                        </Row>
                    </Col>
                    <Col xl={8}>
                        <h5>On Change</h5>
                        <pre>
                            {JSON.stringify(model, null, 4)}
                        </pre>
                    </Col>
                </Row>
            </Content>
        </React.Fragment>
    );
}

export default App;
