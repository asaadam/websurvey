import React from 'react';
import { Col, Row } from 'antd';
import config from './index.config';

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';
import InputText from '../../../components/ant/InputText';
import InputNumber from '../../../components/ant/InputNumber';
import InputTextArea from '../../../components/ant/InputTextArea';
import InputSelect from '../../../components/ant/InputSelect';

const App = () => {
    const { breadcrumb, content } = config;

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content}>
                <Row gutter={12}>
                    <Col lg={12} xl={8}>
                        <InputText text="First Name" />
                    </Col>
                    <Col lg={12} xl={8}>
                        <InputText text="Last Name" />
                    </Col>
                </Row>
                <Row gutter={12}>
                    <Col lg={24} xl={16}>
                        <InputText text="Full Name" />
                    </Col>
                </Row>
                <Row gutter={12}>
                    <Col lg={12} xl={8}>
                        <InputText text="Phone No" />
                    </Col>
                    <Col lg={12} xl={8}>
                        <InputText text="Email" />
                    </Col>
                </Row>
                <Row gutter={12}>
                    <Col lg={12} xl={8}>
                        <InputNumber text="Gross Amount" />
                    </Col>
                    <Col lg={12} xl={8}>
                        <InputNumber text="Net Amount" />
                    </Col>
                </Row>
                <Row gutter={12}>
                    <Col lg={24} xl={16}>
                        <InputSelect text="Company" data={config.data.company} />
                    </Col>
                </Row>
                <Row gutter={12}>
                    <Col lg={24} xl={16}>
                        <InputTextArea text="Address" />
                    </Col>
                </Row>
            </Content>
        </React.Fragment>
    );
}

export default App;
