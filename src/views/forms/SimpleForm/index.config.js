export default {
    breadcrumb: [
        { text: 'Home', to: '/' },
        { text: 'Ant Form', to: '/form/ant' },
        { text: 'Ant Simple Form' },
    ],
    content: {
        title: 'Simple Form',
    },
    model: {
        first_name: '',
        last_name: 'parker'
    },
    data: {
        company: [
            { value: '', text: 'Semua Perusahaan', disabled: true },
            { value: 'apple', text: 'Apple' },
            { value: 'microsoft', text: 'Microsoft' },
            { value: 'gramed', text: 'Gramedia' },
        ],
    }
};