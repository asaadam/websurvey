export default {
    breadcrumb: [
        { text: 'Home', to: '/' },
        { text: 'Ant Form', to: '/form/ant' },
        { text: 'Validation Form' },
    ],
    content: {
        title: 'Validation Form',
        subtitle: 'Sample implementation validation form',
    },
    model: {
        first_name: '',
        last_name: 'parker'
    },
    data: {
        company: [
            { value: '', text: 'Semua Perusahaan', disabled: true },
            { value: 'apple', text: 'Apple' },
            { value: 'microsoft', text: 'Microsoft' },
            { value: 'gramed', text: 'Gramedia' },
        ],
    },
    schema: {
        first_name: {
            label: 'Nama Depan',
            rules: [
                { required: true, message: 'Silahkan input nama depan' },
                { min: 3, message: 'Minimum 3 huruf' },
                { max: 30, message: 'Maximal 30 huruf' },
            ]
        },
        last_name: {
            label: 'Nama Belakang',
            rules: [
                { required: true, message: 'Silahkan input nama belakang' },
            ]
        },
        email: {
            label: 'Email',
            rules: [
                { required: true, message: 'Silahkan input email' },
            ]
        },
        phone_no: {
            label: 'No HP',
            rules: [
                { required: true, message: 'Silahkan No HP' },
            ]
        },
        company: {
            label: 'Perusahaan',
            rules: [
                { required: true, message: 'Silahkan isi perusahaan Anda' },
            ]
        },
        penerbit: {
            label: 'Penerbit',
            rules: [
                { required: true, message: 'Silahkan isi penerbit' },
            ]
        },
        amount1: {
            label: 'Amount Gross',
            rules: [
                { required: true, message: 'Silahkan diisi Amount' },
                { type: 'number', message: 'Harus diisi angka' },
            ]
        },
        amount2: {
            label: 'Amount Nett',
            rules: [
                { required: true, message: 'Silahkan diisi Amount' },
                { type: 'number', message: 'Harus diisi angka' },
            ]
        },
        address: {
            label: 'Address',
            rules: [
                { required: true, message: 'Silahkan input Alamat' },
            ]
        },
        info: {
            label: 'Info',
            rules: [
                { required: true, message: 'Silahkan informasi' },
            ]
        },
    },
};