import React, { useState } from 'react';
import { Col, Row, Button, Form } from 'antd';
import config from './index.config';

import Content from '../../../components/Content';
import Breadcrumb from '../../../components/Breadcrumb';
import InputText from '../../../components/ant/InputText';
import InputNumber from '../../../components/ant/InputNumber';
import InputTextArea from '../../../components/ant/InputTextArea';
import InputSelect from '../../../components/ant/InputSelect';

const App = (props) => {
    const { breadcrumb, content, schema } = config;
    const [binding, setBinding] = useState({});
    const model = binding.values || {};

    const handleSubmit = (e) => {
        e.preventDefault();
        props.form.validateFields((errors, values) => {
            setBinding({ errors, values });
        })
    }

    const handleChange = (name, value) => {
        const { values = {} } = binding;
        values[name] = value;
        setBinding((state) => ({ ...state, values }));
    }

    schema.form = props.form;

    return (
        <React.Fragment>
            <Breadcrumb items={breadcrumb} />
            <Content {...content}>
                <Form onSubmit={handleSubmit}>
                    <Row gutter={24}>
                        <Col xl={16}>
                            <Row gutter={12}>
                                <Col xl={12}>
                                    <InputText name="first_name" schema={schema} onChange={handleChange} />
                                </Col>
                                <Col xl={12}>
                                    <InputText name="last_name" schema={schema} onChange={handleChange} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xl={24}>
                                    <InputText text="Full Name" value={`${model.first_name || ''} ${model.last_name || ''}`} disabled />
                                </Col>
                            </Row>
                            <Row gutter={12}>
                                <Col xl={12}>
                                    <InputText name="phone_no" schema={schema} />
                                </Col>
                                <Col xl={12}>
                                    <InputText name="email" schema={schema} />
                                </Col>
                            </Row>
                            <Row gutter={12}>
                                <Col xl={12}>
                                    <InputNumber name="amount1" schema={schema} />
                                </Col>
                                <Col xl={12}>
                                    <InputNumber name="amount2" schema={schema} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xl={24}>
                                    <InputSelect name="company" data={config.data.company} schema={schema} />
                                </Col>
                            </Row>
                            <Row>
                                <Col xl={24}>
                                    <InputTextArea name="address" schema={schema} />
                                </Col>
                            </Row>
                        </Col>
                        <Col xl={8}>
                            <h5>On Change</h5>
                            <pre>
                                {JSON.stringify(binding.values, null, 4)}
                            </pre>
                        </Col>
                    </Row>
                    <hr />
                    <Button htmlType="submit">Submit</Button>
                </Form>
            </Content>
        </React.Fragment>
    );
}

export default Form.create({ name: 'validation_form' })(App);