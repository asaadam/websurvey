
function goToRight(N, numbers, meetZero) {
  console.log(numbers);
  if (!meetZero) {
    let temp = numbers - 5;
    if (temp <= 0) {
      console.log(" ");
      meetZero = true;
    }
    goToRight(N, temp, meetZero);
  }
  else {
    let temp = numbers + 5;
    if (temp === N) {
      console.log(temp);
      return;
    }
    else {
      goToRight(N, temp, meetZero);
    }
  }
}

goToRight(16, 16, false);