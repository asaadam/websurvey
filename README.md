# Introduction
moco_dashboard_core is a base framework for all __Aksaramaya's Dashboard__ products from various Services, we use React as based because React allows developers to create large web applications which can change data, without reloading the page. The main purpose of React is to be fast, scalable, and simple.

We will have another repository for sample implementation of __moco_dashboard_core__, in that implementation we will showcase various case which have been implemented before, at it will always be improved

moco_dashboard_core is based on the following frameworks:
* Material UI 3.0 https://material-ui.com/ 
* and many more ...

# Getting Started #
To get started you just need to
* clone this repository
* ```cd moco_dashboard_sample```
* ```git checkout starter```
* ```npm install```
* ``npm start``

you can view it on localhost:3000


# Features List #
- [x] Skeleton Project (branch starter)
- [x] Sample Project (branch master)
- [x] Custom Layout
- [x] Multiple Data Provider
- [x] Internationalization
- [x] Google Firebase Integration

## Custom Layout
 - moco
 - full-page ('inprogress') 

## Data Provider
 - internal api
 - external api

## Internationalization
 - manageable language

# Components
## Breadcrumb
## Content
## InputControl
 - InputText
 - InputTextArea
 - InputSelect
 - InputNumber
 - ImageCheck
## InputForm


